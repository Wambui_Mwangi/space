<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Apartment extends Model
{
    //
    protected $fillable = ['image', 'location', 'details', 'units'];

    public function rates()
    {
        return $this->hasMany('App\Rate');
    }
}
