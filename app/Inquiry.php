<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inquiry extends Model
{
    //

    protected $fillable = [
        'name', 'tenant_id', 'message'
    ];
    
    public function tenant()
    {
    	return $this->belongsTo('App\User', 'tenant_id');
    }
}
