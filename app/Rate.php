<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    //
    protected $fillable = [
        'amount', 'apartment_id', 'duration', 'space_id'
    ];

    public function apartment()
    {
    	return $this->belongsTo('App\Apartment');
    }

    public function space()
    {
    	return $this->belongsTo('App\Space');
    }
}
