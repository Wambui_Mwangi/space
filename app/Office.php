<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
    //
    protected $fillable = [
        'dimensions', 'location', 'pin', 'image', 'price', 'category_id'
    ];

    public function category()
    {
    	return $this->belongsTo('App\Category');
    }
}
