<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rent extends Model
{
    //
	protected $table = 'rent';

    protected $fillable = [
        'month', 'occupancy_id', 'year', 'amount'
    ];

    public function occupancy()
    {
    	return $this->belongsTo('App\Occupancy', 'occupancy_id');
    }
}
