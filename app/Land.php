<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Land extends Model
{
    //
    protected $fillable = [
        'dimensions', 'location', 'pin', 'lower_price', 'upper_price', 'image'
    ];
}
