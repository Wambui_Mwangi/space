<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    //
    protected $fillable = ['name', 'category_id', 'county_id', 'user_id', 'location', 'description', 'price', 'featured', 'image'];

    public function county()
    {
    	return $this->belongsTo('App\County');
    }

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function category()
    {
    	return $this->belongsTo('App\Category');
    }

    public function tenants()
    {
        return $this->hasMany('App\Tenant');
    }

    public function rooms()
    {
        return $this->hasMany('App\Room', 'property_id');
    }
}
