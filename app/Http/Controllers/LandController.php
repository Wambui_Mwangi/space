<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Land;

class LandController extends Controller
{
    //

    public function index()
    {
    	$lands = Land::all();

    	return view('backend.lands.index', ['lands' => $lands]);
    }

    public function add()
    {
    	return view('backend.lands.add');
    }

    public function store(Request $request)
    {
        // dd($request->all());

        //validate data
        $validatedData = $request->validate([
          	'location' => 'required',
          	'dimensions' => 'required',
          	'lower_price' => 'required',
          	'upper_price' => 'required',
          	'image' => 'required',
          	'pin' => 'required',
          ]);

        //Save image
        $image = $request->file('image');
        $imageName=time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/img');
        $image->move($destinationPath, $imageName);

        //save land data
        $land = new land;

        $land->location = $request->location;
        $land->dimensions = $request->dimensions;
        $land->lower_price = $request->lower_price;
        $land->image = $imageName;
        $land->pin = $request->pin;
        $land->upper_price = $request->upper_price;
         
        $land->save();

        return redirect()->route('lands');

    }

    public function edit($id)
    {
    	$land = land::find($id);


    	return view('backend.lands.edit', ['land' => $land]);
    }

    public function update(Request $request, $id)
    {
    	// dd($request->all());

    	$validatedData = $request->validate([
          	'location' => 'required',
          	'dimensions' => 'required',
          	'lower_price' => 'required',
          	'upper_price' => 'required',
          	'pin' => 'required',
          ]);

    	$land = land::find($id);

    	//save image
    	if($request->image)
    	{
    		//Save image
        $image = $request->file('image');
        $imageName=time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/img');
        $image->move($destinationPath, $imageName);

        

        $land->location = $request->location;
        $land->dimensions = $request->dimensions;
        $land->lower_price = $request->lower_price;
        $land->image = $imageName;
        $land->pin = $request->pin;
        $land->upper_price = $request->upper_price;
    	}

    	else
    	{
	        $land->location = $request->location;
	        $land->dimensions = $request->dimensions;
	        $land->lower_price = $request->lower_price;
	        $land->pin = $request->pin;
	        $land->upper_price = $request->upper_price;
    	}

    	
         
        $land->save();

        return redirect()->route('lands');


    }

    public function delete($id)
    {
    	$land = Land::find($id);
    	$land->delete();

    	return redirect()->route('lands');
    }
}
