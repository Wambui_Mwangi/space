<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Category;
use App\County;
use App\User;
use App\Property;
use App\Room;


class RoomController extends Controller
{
    //
    public function index($id)
    {
      	$rooms = Room::where('property_id', $id)->get();
      	$property = Property::find($id);

      	// dd($property);
    	return view('backend.property.rooms.index', ['rooms' => $rooms, 'property' => $property]);
    }

    public function add($id)
    {
    	$property = Property::find($id);
    	return view('backend.property.rooms.add', ['property' => $property]);
    }

    public function store(Request $request, $id)
    {
    	// dd($request->all());

    	$validatedData = $request->validate([
          	'name' => 'required',
          ]);

          $room = new Room;

          $room->name = $request->name;
          $room->property_id = $id;
         
          $room->save();

          $property = Property::find($id);
          $rooms = Room::where('property_id', $id)->get();

          // dd($property);
      

          return redirect()->route('rooms', ['id' => $property->id]);
    }

    public function edit($id)
    {
    	$room = Room::find($id);

    	$data = [
    		'room' => $room,
    	];
    	
    	return view('backend.property.rooms.edit', $data);
    }

    public function delete($id)
    {
    	$room = Room::find($id);
    	$room->delete();

    	return redirect()->route('rooms', ['id' => $room->property_id]);
    }

    public function update(Request $request, $id)
    {
    	// dd($request->all());
    	$validatedData = $request->validate([
          	'name' => 'required',
          ]);

    	$room = Room::find($id);
 
      	$room->name = $request->name;
		$room->save();      

      return redirect()->route('rooms', ['id' => $room->property_id]);
    }
}
