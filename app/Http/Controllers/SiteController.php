<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Property;
use App\County;
use App\Office;
use App\Space;
use App\Land;

class SiteController extends Controller
{
    //

    public function welcome()
    {
        $properties = Property::inRandomOrder()->get();
        $counties = County::all();
        $offices = Office::all();
        $spaces = Space::all();
        $lands = Land::all();
        $data = 
        [
            'properties' => $properties, 
            'counties' => $counties,
            'offices' => $offices,
            'spaces' => $spaces,
            'lands' => $lands,
        ];
    	return view('frontend.home', $data);
    }

    public function about()
    {
    	return view('frontend.about');
    }

    public function featured()
    {   $properties = Property::where('featured', 1)->get();
    	return view('frontend.featured', ['properties' => $properties]);
    }

    public function tenants()
    {
    	return view('frontend.tenants');
    }

    public function owners()
    {
    	return view('frontend.owners');
    }

    public function contact()
    {
    	return view('frontend.contact');
    }

    public function search(Request $request)
    {
        // dd($request->all());
        $results = Property::where('description', 'like', '%'.$request->specifications.'%')
                        ->orWhere('location', 'like', '% '. $request->specifications . '%')
                        ->orWhere('price', 'like', '% '. $request->specifications . '%')
                        ->get();
        // dd($results);

        return view('frontend.search', ['results' => $results]);
    }

    public function services()
    {
        return view('frontend.services');
    }

    public function viewProperties()
    {
        $properties = Property::all();
        return view('frontend.properties', ['properties' => $properties]);
    }

    public function viewOffices()
    {
        $offices = Office::all();
        return view('frontend.offices', ['offices' => $offices]);
    }  

    public function viewSpaces()
    {
        $spaces = Space::all();
        return view('frontend.spaces', ['spaces' => $spaces]);
    }  

    public function viewLand()
    {
        $lands = Land::all();
        return view('frontend.land', ['lands' => $lands]);
    }

    public function viewSingleProperties($id)
    {
        $property = Property::find($id);
        return view('frontend.single_property', ['property' => $property]);
    }

    public function viewSingleOffices($id)
    {
        $office = Office::find($id);
        return view('frontend.single_offices', ['office' => $office]);
    }  

    public function viewSingleSpaces($id)
    {
        $space = Space::find($id);
        return view('frontend.single_spaces', ['space' => $space]);
    }  

    public function viewSingleLand($id)
    {
        $land = Land::find($id);
        return view('frontend.single_land', ['land' => $land]);
    }
}
