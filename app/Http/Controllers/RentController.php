<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Occupancy;
use App\Rent;

class RentController extends Controller
{
    //

    public function index($id)
    {
      	$rents = Rent::where('occupancy_id', $id)->get();
      	$occupant = Occupancy::find($id);

      	// dd($property);
    	return view('backend.rent.index', ['rents' => $rents, 'occupant' => $occupant]);
    }

    public function add($id)
    {
    	$occupancy = Occupancy::find($id);
    	$year = date('Y');
    	return view('backend.rent.add', ['occupancy' => $occupancy, 'year' => $year]);
    }

    public function store(Request $request, $id)
    {
    	// dd($request->all());

    	$validatedData = $request->validate([
          	'month' => 'required',
          	'year' => 'required',
          	'amount' => 'required',
          	'details' => 'required',
          ]);

          $rent = new Rent;

          $rent->amount = $request->amount;
          $rent->year = $request->year;
          $rent->month = $request->month;
          $rent->details = $request->details;
          $rent->occupancy_id = $id;
         
          $rent->save();

         
          // dd($property);
      

          return redirect()->route('rent', ['id' => $id]);
    }
}
