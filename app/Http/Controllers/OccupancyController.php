<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Occupancy;
use App\User;
use App\Room;

class OccupancyController extends Controller
{
    //
    public function index($id)
    {
      	$occupants = Occupancy::where('room_id', $id)->get();
      	$room = Room::find($id);

      	// dd($property);
    	return view('backend.occupancy.index', ['occupants' => $occupants, 'room' => $room]);
    }

    public function add($id)
    {
    	$room = Room::find($id);
    	$tenants = User::where('role', 'tenant')->get();
    	return view('backend.occupancy.add', ['room' => $room, 'tenants' => $tenants]);
    }

    public function store(Request $request, $id)
    {
    	// dd($request->all());

    	$validatedData = $request->validate([
          	'tenant_id' => 'required',
          	'month' => 'required',
          ]);

          $occupancy = new Occupancy;

          $occupancy->tenant_id = $request->tenant_id;
          $occupancy->room_id = $id;
          $occupancy->month = $request->month;

          $occupancy->save();

          // dd($property);
      

          return redirect()->route('occupancy', ['id' => $id]);
    }
}
