<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\User;
use App\Property;
use App\Tenant;
use App\Agent;
use App\Occupancy;

class AdminController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function addUser()
    {
    	return view('backend.users.add');
    }


    public function storeUser(Request $request)
    {
    	// dd($request->all());

    	$email = $request->email;

    		$user = User::create([
            'name' => Input::get('name'),
            'email' => Input::get('email'),
            'password' => bcrypt(Input::get('email')),
            'telephone' => Input::get('telephone'),
            'role' => Input::get('role'),
        	]);

            $data = [
                'name' => $request->name,
                'password' => $email,
                'title' => 'Space and Occupy account for '. $request->name,
                
                 ];
        if($user->role == "agent")
        {
            Mail::send('emails.agent_send', $data, function ($message) use ($email)
            {
                $message->subject('New account created!');
                $message->from('info@spaceoccupy.org', 'Space and Occupy admin');
                $message->to($email);

            });
        }
        elseif($user->role == "owner")
        {
        	Mail::send('emails.owner_send', $data, function ($message) use ($email)
            {
                $message->subject('New account created!');
                $message->from('info@spaceoccupy.org', 'Space and Occupy admin');
                $message->to($email);

            });
        }
        else
        {
            Mail::send('emails.tenant_send', $data, function ($message) use ($email)
            {
                $message->subject('New account created!');
                $message->from('info@spaceoccupy.org', 'Space and Occupy admin');
                $message->to($email);

            });
        }
            
    		return redirect()->route('users');
  	

    }

    public function editUser($id)
    {
        $user = User::find($id);
        // dd($user);

        return view('backend.users.edit', ['user' => $user]);
    }


    public function allUsers()
    {
        $users = User::all();

        return view('backend.users.all', ['users' => $users]);
    }

    public function deactivateUser($id)
    {
        $user = User::find($id);
        $user->is_active = !$user->is_active;
        $user->save();

        return redirect()->route('users');
    }

    public function addTenant()
    {
        $properties = Property::all();
        return view('backend.users.tenants.add', ['properties' => $properties]);
    }

    public function allTenants()
    {
        if(Auth::user()->role == 'admin')
        {
            $tenants = User::where('role', 'tenant')->get();
        }
        elseif(Auth::user()->role == 'admin')
        {
            $tenants = User::leftjoin('occupancy', 'users.id', '=', 'occupancy.tenant_id')
                ->leftjoin('rooms', 'occupancy.room_id', '=', 'rooms.id')
                ->leftjoin('properties', 'rooms.property_id', 'properties.id')
                ->where('properties.agent_id', Auth::id())
                ->select('users.*')
                ->get();
        }
        else
        {
            $tenants = User::leftjoin('occupancy', 'users.id', '=', 'occupancy.tenant_id')
                ->leftjoin('rooms', 'occupancy.room_id', '=', 'rooms.id')
                ->leftjoin('properties', 'rooms.property_id', 'properties.id')
                ->where('properties.owner_id', Auth::id())
                ->select('users.*')
                ->get();
        }

        // dd($tenants);
        return view('backend.users.tenants.all', ['tenants' => $tenants]);
    }

    public function storeTenant(Request $request)
    {
        // dd($request->all());

      

            $user = Tenant::create([
            'name' => Input::get('name'),
            'email' => Input::get('email'),
            'telephone' => Input::get('telephone'),
            'property_id' => Input::get('property_id'),
            ]);

        
            
            return redirect()->route('tenants');
    

    }

    public function editTenant($id)
    {
        $tenant = Tenant::find($id);
        $properties = Property::all();

        return view('backend.users.tenant.edit', ['tenant' => $tenant, 'properties' => $properties]);
    }

    public function updateTenant(Request $request, $id)
    {
        $tenant = Tenant::find($id);

        $tenant->name = $request->name;
        $tenant->email = $request->email;
        $tenant->telephone = $request->telephone;
        $tenant->property_id = $request->property_id;
        $tenant->save();

        return redirect()->route('tenants');

    }

    // Agents in the system

    public function allAgents()
    {
        $agents = User::where('role', 'agent')->get();
                

        // dd($agents);
        return view('backend.users.agents.all', ['agents' => $agents]);
    }

    public function addAgent()
    {
        $properties = Property::all();
        return view('backend.users.agents.add', ['properties' => $properties]);
    }

    //Owners in the system

    public function allOwners()
    {
        $user = Auth::user();

        $owners = User::where('role', 'owner')->get();
                

        // dd($owners);
        return view('backend.users.owners.all', ['owners' => $owners]);
    }


}
