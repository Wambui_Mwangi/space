<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Space;
use App\Category;
use App\Rate;

class SpaceController extends Controller
{
    //
    public function index()
    {
    	$spaces = Space::all();

    	return view('backend.spaces.index', ['spaces' => $spaces]);
    }

    public function add()
    {
	   	return view('backend.spaces.add');
    }

    public function store(Request $request)
    {
        // dd($request->all());

        //validate data
        $validatedData = $request->validate([
          	'location' => 'required',
          	'dimensions' => 'required',
          	'image' => 'required',
          	'pin' => 'required',
          ]);

        //Save image
        $image = $request->file('image');
        $imageName=time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/img');
        $image->move($destinationPath, $imageName);

        //save space data
        $space = new Space;

        $space->location = $request->location;
        $space->dimensions = $request->dimensions;
        $space->image = $imageName;
        $space->pin = $request->pin;
         
        $space->save();

        //create rates
        $amounts = Input::get('amount');
        $durations = Input::get('duration');


        $keys = array_keys($amounts);

        // dd($keys);
        

        foreach($keys as $key)
        {
            $rate = Rate::create([
                'amount' => $amounts[$key],
                'space_id' => $space->id,
                'duration' => $request->duration[$key],
            ]);
        }

       


        return redirect()->route('spaces');

    }

    public function edit($id)
    {
    	$categories = Category::all();
    	$space = space::find($id);
        $rates = Rate::where('space_id', $id)->get();


    	return view('backend.spaces.edit', ['space' => $space, 'categories' => $categories, 'rates' => $rates]);
    }

    public function update(Request $request, $id)
    {
    	// dd($request->all());

    	$validatedData = $request->validate([
          	'location' => 'required',
          	'dimensions' => 'required',
          	'pin' => 'required',
          ]);

    	$space = space::find($id);

    	//save image
    	if($request->image)
    	{
    		//Save image
        $image = $request->file('image');
        $imageName=time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/img');
        $image->move($destinationPath, $imageName);

        

        $space->location = $request->location;
        $space->dimensions = $request->dimensions;
        $space->image = $imageName;
        $space->pin = $request->pin;
        
    	}

    	else
    	{
	        $space->location = $request->location;
	        $space->dimensions = $request->dimensions;
	        $space->pin = $request->pin;
	        
    	}

    	//create rates
        $amounts = Input::get('amount');
        $durations = Input::get('duration');


        $keys = array_keys($amounts);

        // dd($keys);
        

        foreach($keys as $key)
        {
            $rate = Rate::create([
                'amount' => $amounts[$key],
                'space_id' => $space->id,
                'duration' => $request->duration[$key],
            ]);
        }

         
        $space->save();

        return redirect()->route('spaces');


    }

    public function delete($id)
    {
    	$space = Space::find($id);
    	$space->delete();

    	return redirect()->route('spaces');
    }
}
