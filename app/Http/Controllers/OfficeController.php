<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Office;
use App\Category;
use App\Rate;

class OfficeController extends Controller
{
    //
    public function index()
    {
    	$offices = Office::all();

    	return view('backend.offices.index', ['offices' => $offices]);
    }

    public function add()
    {
    	$categories = Category::all();
    	return view('backend.offices.add', ['categories' => $categories]);
    }

    public function store(Request $request)
    {
        // dd($request->all());

        //validate data
        $validatedData = $request->validate([
          	'location' => 'required',
          	'dimensions' => 'required',
          	'price' => 'required',
          	'image' => 'required',
          	'pin' => 'required',
          	'category_id' => 'required',
          ]);

        //Save image
        $image = $request->file('image');
        $imageName=time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/img');
        $image->move($destinationPath, $imageName);

        //save office data
        $office = new Office;

        $office->location = $request->location;
        $office->dimensions = $request->dimensions;
        $office->price = $request->price;
        $office->image = $imageName;
        $office->pin = $request->pin;
        $office->category_id = $request->category_id;
         
        $office->save();

       


        return redirect()->route('offices');

    }

    public function edit($id)
    {
    	$categories = Category::all();
    	$office = Office::find($id);


    	return view('backend.offices.edit', ['office' => $office, 'categories' => $categories]);
    }

    public function update(Request $request, $id)
    {
    	// dd($request->all());

    	$validatedData = $request->validate([
          	'location' => 'required',
          	'dimensions' => 'required',
          	'price' => 'required',
          	'pin' => 'required',
          	'category_id' => 'required',
          ]);

    	$office = office::find($id);

    	//save image
    	if($request->image)
    	{
    		//Save image
        $image = $request->file('image');
        $imageName=time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/img');
        $image->move($destinationPath, $imageName);

        

        $office->location = $request->location;
        $office->dimensions = $request->dimensions;
        $office->price = $request->price;
        $office->image = $imageName;
        $office->pin = $request->pin;
        $office->category_id = $request->category_id;
        
    	}

    	else
    	{
	        $office->location = $request->location;
	        $office->dimensions = $request->dimensions;
	        $office->price = $request->price;
	        $office->pin = $request->pin;
        	$office->category_id = $request->category_id;
	        
    	}

         
        $office->save();

        return redirect()->route('offices');


    }

    public function delete($id)
    {
    	$office = Office::find($id);
    	$office->delete();

    	return redirect()->route('offices');
    }
}
