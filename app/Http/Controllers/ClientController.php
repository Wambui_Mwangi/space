<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;

class ClientController extends Controller
{
    //
    public function index()
    {
    	$clients = Client::all();

    	return view('backend.clients.index', ['clients' => $clients]);
    }

    public function store(Request $request)
    {
    	// dd($request->all());

    	$validatedData = $request->validate([
          	'message' => 'required',
          	'name' => 'required',
          	'email' => 'required',
          	'telephone' => 'required',
          ]);

          $client = new Client;

          $client->message = $request->message;
          $client->name = $request->name;
          $client->telephone = $request->telephone;
          $client->email = $request->email;
         
          $client->save();
      

          return redirect()->route('contact_us')->with('success', 'Your message has been received. We will get back to you as soon as we can.');
    }
}
