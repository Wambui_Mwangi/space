<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Apartment;
use App\Rate;
use Illuminate\Support\Facades\Input;


class ApartmentController extends Controller
{
    //

    public function index()
    {
    	$apartments = Apartment::all();

    	return view('backend.apartments.index', ['apartments' => $apartments]);
    }

    public function add()
    {
    	return view('backend.apartments.add');
    }

    public function store(Request $request)
    {
        // dd($request->all());

        //validate data
        $validatedData = $request->validate([
          	'location' => 'required',
          	'details' => 'required',
          	'units' => 'required',
          	'duration' => 'required',
          	'image' => 'required',
          	'amount' => 'required',
          	'pin' => 'required',
          ]);

        //Save image
        $image = $request->file('image');
        $imageName=time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/img');
        $image->move($destinationPath, $imageName);

        //save apartment data
        $apartment = new Apartment;

        $apartment->location = $request->location;
        $apartment->details = $request->details;
        $apartment->units = $request->units;
        $apartment->image = $imageName;
        $apartment->pin = $request->pin;
         
        $apartment->save();

        //create rates
        $amounts = Input::get('amount');
        $durations = Input::get('duration');


        $keys = array_keys($amounts);

        // dd($keys);
        

        foreach($keys as $key)
        {
            $rate = Rate::create([
                'amount' => $amounts[$key],
                'apartment_id' => $apartment->id,
                'duration' => $request->duration[$key],
            ]);
        }

        return redirect()->route('apartments');

    }

    public function edit($id)
    {
    	$apartment = Apartment::find($id);
    	$rates = Rate::where('apartment_id', $id)->get();

    	// dd($rates);

    	return view('backend.apartments.edit', ['apartment' => $apartment, 'rates' => $rates]);
    }

    public function update(Request $request, $id)
    {
    	// dd($request->all());

    	$validatedData = $request->validate([
          	'location' => 'required',
          	'details' => 'required',
          	'units' => 'required',
          	'duration' => 'required',
          	'amount' => 'required',
          	'pin' => 'required',
          ]);

    	$apartment = Apartment::find($id);

    	//save image
    	if($request->image)
    	{
    		//Save image
        $image = $request->file('image');
        $imageName=time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/img');
        $image->move($destinationPath, $imageName);

        

        $apartment->location = $request->location;
        $apartment->details = $request->details;
        $apartment->units = $request->units;
        $apartment->image = $imageName;
        $apartment->pin = $request->pin;
    	}

    	else
    	{
	        $apartment->location = $request->location;
	        $apartment->details = $request->details;
	        $apartment->units = $request->units;
	        $apartment->pin = $request->pin;
    	}

    	
         
        $apartment->save();

        //create rates
        $amounts = Input::get('amount');
        $durations = Input::get('duration');


        $keys = array_keys($amounts);

        // dd($keys);
        

        foreach($keys as $key)
        {
            $rate = Rate::updateOrCreate([
                'amount' => $amounts[$key],
                'apartment_id' => $apartment->id,
                'duration' => $request->duration[$key],
            ]);
        }

        return redirect()->route('apartments');


    }

    public function delete($id)
    {
    	$apartment = Apartment::find($id);
    	$apartment->delete();

    	return redirect()->route('apartments');
    }
}
