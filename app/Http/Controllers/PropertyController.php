<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Category;
use App\County;
use App\User;
use App\Property;
use App\Room;


class PropertyController extends Controller
{
    //
    public function index()
    {
      $user = Auth::user();
      if($user->role == 'admin'){
    	$properties = Property::all();
      }
      elseif($user->role == 'agent') 
      {
        $properties = Property::where('agent_id', Auth::id())->get();
      }
      else
      {
        $properties = Property::where('owner_id', Auth::id())->get();
      }

      // dd($properties);
    	return view('backend.property.index', ['properties' => $properties, 'user' => $user]);
    }

    public function add()
    {
    	$counties = County::all();
    	$categories = Category::all();
    	$owners = User::where('role', 'owner')->get();
      $agents = User::where('role', 'agent')->get();
    	$data = [
    		'counties' => $counties,
    		'categories' => $categories,
    		'owners' => $owners,
        'agents' => $agents,
    	];

      // dd($data);
    	return view('backend.property.add', $data);
    }

    public function store(Request $request)
    {
    	// dd($request->all());

    	$validatedData = $request->validate([
          	'category_id' => 'required',
          	'county_id' => 'required',
          	'price' => 'required',
          	'description' => 'required',
          	'location' => 'required',
          	'owner_id' => 'required',
            'agent_id' => 'required'
          ]);

    	$image = $request->file('image');
        $imageName=time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/img');
        $image->move($destinationPath, $imageName);

          

          $property = new Property;

          $property->name = $request->name;
          $property->category_id = $request->category_id;
          $property->county_id = $request->county_id;
          $property->owner_id = $request->owner_id;
          $property->agent_id = $request->agent_id;
          $property->price = $request->price;
          $property->location = $request->location;
          $property->description = $request->description;
          $property->image = $imageName;
          $property->save();

          // dd($property);
      

      return redirect()->route('properties');
    }

    public function feature($id)
    {
    	$property = Property::find($id);
    	$property->featured = !$property->featured;
    	$property->save();
    	return redirect()->route('properties');
    }

    public function edit($id)
    {
    	$property = Property::find($id);
    	$counties = County::all();
    	$categories = Category::all();
    	$users = User::all();
    	$data = [
    		'counties' => $counties,
    		'categories' => $categories,
    		'users' => $users,
    		'property' => $property,
    	];
    	
    	return view('backend.property.edit', $data);
    }

    public function delete($id)
    {
    	$property = Property::find($id);
    	$property->delete();

    	return redirect()->route('properties');
    }

    public function update(Request $request, $id)
    {
    	// dd($request->all());
    	$validatedData = $request->validate([
          	'category_id' => 'required',
          	'county_id' => 'required',
          	'price' => 'required',
          	'description' => 'required',
          	'location' => 'required',
          	'owner_id' => 'required',
            'agent_id' => 'required'
          ]);

    	$property = Property::find($id);

    	if($request->image)
    	{
	    	$image = $request->file('image');
	        $imageName=time().'.'.$image->getClientOriginalExtension();
	        $destinationPath = public_path('/img');
	        $image->move($destinationPath, $imageName);
	        $property->image = $imageName;
    	}
          

          

          $property->category_id = $request->category_id;
          $property->county_id = $request->county_id;
          $property->owner_id = $request->owner_id;
          $property->agent_id = $request->agent_id;
          $property->price = $request->price;
          $property->location = $request->location;
          $property->description = $request->description;
          
          $property->save();

          // dd($property);
      

      return redirect()->route('properties');
    }

    public function allRooms($id)
    {
      $rooms = Room::where('property_id', $id)->get();

      return view('backend.property.rooms.index', ['rooms' => $rooms]);
    }
}
