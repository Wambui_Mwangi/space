<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Inquiry;

class InquiryController extends Controller
{
    //
    public function tenantContact()
    {
    	return view('backend.inquiries.add');
    }

    public function store(Request $request)
    {
    	// dd($request->all());

    	$validatedData = $request->validate([
          	'message' => 'required',
          ]);

          $inquiry = new Inquiry;

          $inquiry->message = $request->message;
          $inquiry->tenant_id = Auth::id();
         
          $inquiry->save();
      

          return redirect()->route('inquiry')->with('success', 'Your message has been received. We will get back to you as soon as we can.');
    }

    public function viewInquiries()
    {
    	if(Auth::user()->role == 'admin')
    	{
    		$inquiries = Inquiry::all();
    	}
    	elseif(Auth::user()->role == 'agent')
    	{
    		$inquiries = Inquiry::leftjoin('occupancy', 'inquiries.tenant_id', '=', 'occupancy.tenant_id')
    			->leftjoin('rooms', 'occupancy.room_id', '=', 'rooms.id')
    			->leftjoin('properties', 'rooms.property_id', 'properties.id')
    			->leftjoin('users', 'properties.agent_id', '=', 'users.id')
    			->where('users.id', Auth::id())
    			->get();
    	}
    	else
    	{
    		$inquiries = Inquiry::leftjoin('occupancy', 'inquiries.tenant_id', '=', 'occupancy.tenant_id')
    			->leftjoin('rooms', 'occupancy.room_id', '=', 'rooms.id')
    			->leftjoin('properties', 'rooms.property_id', 'properties.id')
    			->leftjoin('users', 'properties.owner_id', '=', 'users.id')
    			->where('users.id', Auth::id())
    			->get();
    	}

    	// dd($inquiries);

    	return view('backend.inquiries.view', ['inquiries' => $inquiries]);
    }
}
