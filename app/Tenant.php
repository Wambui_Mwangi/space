<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tenant extends Model
{
    //
    protected $fillable = [
        'name', 'email', 'property_id', 'telephone', 
    ];

    public function property()
    {
    	return $this->belongsTo('App\Property');
    }

    public function occupancy()
    {
        return $this->hasOne('App\Occupancy', 'occupancy_id');
    }

}
