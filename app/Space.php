<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Space extends Model
{
    //
    protected $fillable = [
        'dimensions', 'location', 'pin', 'image',
    ];

    public function rates()
    {
        return $this->hasMany('App\Rate');
    }
}
