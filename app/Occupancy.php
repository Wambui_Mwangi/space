<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Occupancy extends Model
{
    //
    protected $table = 'occupancy';

    protected $fillable = ['tenant_id', 'room_id', 'month'];

    public function room()
    {
    	return $this->belongsTo('App\Room', 'room_id');
    }

    public function tenant()
    {
    	return $this->belongsTo('App\User', 'tenant_id');
    }

    public function rent()
    {
        return $this->hasMany('App\Rent', 'occupancy_id');
    }
}
