<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    //
    protected $fillable = [
        'name', 'property_id'
    ];

    public function property()
    {
    	return $this->belongsTo('App\Property', 'property_id');
    }

    public function occupancies()
    {
        return $this->hasMany('App\Occupancy', 'room_id');
    }
}
