<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'is_admin', 'telephone', 'role'
    ];

    /**
     * The guard to be used.
     *
     * @var array
     */
    protected $guard_name = 'auth';


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function properties()
    {
        return $this->hasMany('App\Property');
    }

    public function occupancy()
    {
        return $this->hasOne('App\Occupancy', 'tenant_id');
    }

    public function inquiries()
    {
        return $this->hasMany('App\Inquiry', 'tenant_id');
    }
}
