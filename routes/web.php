<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

//FRONTEND ROUTES
//Navbar routes
Route::get('/', 'SiteController@welcome')->name('welcome');
Route::get('about-us', 'SiteController@about')->name('about_us');
Route::get('featured-properties', 'SiteController@featured')->name('featured_properties');
Route::get('all-properties', 'SiteController@properties')->name('all_properties');
Route::get('tenants-information', 'SiteController@tenants')->name('tenants_information');
Route::get('owners-information', 'SiteController@owners')->name('owners_information');
Route::get('owners-frequently-asked-questions', 'SiteController@ownersfaqs')->name('owners_faqs');
Route::get('tenants-frequently-asked-questions', 'SiteController@tenantsfaqs')->name('tenants_faqs');
Route::get('contact-us', 'SiteController@contact')->name('contact_us');
Route::get('services', 'SiteController@services')->name('services');

//Search
Route::post('search', 'SiteController@search')->name('search');

//Properties
Route::get('view-properties', 'SiteController@viewProperties')->name('view_properties');
Route::get('view-offices', 'SiteController@viewOffices')->name('view_offices');
Route::get('view-spaces', 'SiteController@viewSpaces')->name('view_spaces');
Route::get('view-land', 'SiteController@viewLand')->name('view_land');

//Single Properties
Route::get('view-single-properties/{id}', 'SiteController@viewSingleProperties')->name('view_single_properties');
Route::get('view-single-offices/{id}', 'SiteController@viewSingleOffices')->name('view_single_offices');
Route::get('view-single-spaces/{id}', 'SiteController@viewSingleSpaces')->name('view_single_spaces');
Route::get('view-single-land/{id}', 'SiteController@viewSingleLand')->name('view_single_land');

// BACKEND ROUTES

//Side navigation bar routes
Route::get('home', 'HomeController@index')->name('home');
Route::get('properties', 'PropertyController@index')->name('properties');
Route::get('users', 'AdminController@allUsers')->name('users');
Route::get('tenants', 'AdminController@allTenants')->name('tenants');
Route::get('agents', 'AdminController@allAgents')->name('agents');
Route::get('owners', 'AdminController@allOwners')->name('owners');
Route::get('apartments', 'ApartmentController@index')->name('apartments');
Route::get('lands', 'LandController@index')->name('lands');
Route::get('offices', 'OfficeController@index')->name('offices');
Route::get('spaces', 'SpaceController@index')->name('spaces');
Route::get('clients', 'ClientController@index')->name('clients');




//Properties routes
Route::get('add-property', 'PropertyController@add')->name('add_property');
Route::post('store-property', 'PropertyController@store')->name('store_property');
Route::get('feature-property/{id}', 'PropertyController@feature')->name('feature_property');
Route::get('edit-property/{id}', 'PropertyController@edit')->name('edit_property');
Route::get('delete-property/{id}', 'PropertyController@delete')->name('delete_property');
Route::post('update-property/{id}', 'PropertyController@update')->name('update_property');


//Administrator routes
//Users
Route::get('add-user', 'AdminController@addUser')->name('add_user');
Route::post('store-user', 'AdminController@storeUser')->name('store_user');
Route::get('edit-user/{id}', 'AdminController@editUser')->name('edit_user');
Route::post('update-user/{id}', 'AdminController@updateUser')->name('update_user');
Route::get('deactivate-user/{id}', 'AdminController@deactivateUser')->name('deactivate_user');

//Tenants
Route::get('add-tenant', 'AdminController@addTenant')->name('add_tenant');
Route::post('store-tenant', 'AdminController@storeTenant')->name('store_tenant');
Route::get('edit-tenant/{id}', 'AdminController@editTenant')->name('edit_tenant');
Route::post('update-tenant/{id}', 'AdminController@updateTenant')->name('update_tenant');

//Agents
Route::get('add-agent', 'AdminController@addAgent')->name('add_agent');
Route::post('store-agent', 'AdminController@storeAgent')->name('store_agent');
Route::get('edit-agent/{id}', 'AdminController@editAgent')->name('edit_agent');
Route::post('update-agent/{id}', 'AdminController@updateAgent')->name('update_agent');


//Occupancy
Route::get('occupancy/{id}', 'OccupancyController@index')->name('occupancy');
Route::get('add-occupancy/{id}', 'OccupancyController@add')->name('add_occupancy');
Route::post('store-occupancy/{id}', 'OccupancyController@store')->name('store_occupancy');
Route::get('edit-occupancy/{id}', 'OccupancyController@edit')->name('edit_occupancy');
Route::get('delete-occupancy/{id}', 'OccupancyController@delete')->name('delete_occupancy');
Route::post('update-occupancy/{id}', 'OccupancyController@update')->name('update_occupancy');

//Rooms
Route::get('rooms/{id}', 'RoomController@index')->name('rooms');
Route::get('add-room/{id}', 'RoomController@add')->name('add_room');
Route::post('store-room/{id}', 'RoomController@store')->name('store_room');
Route::get('edit-room/{id}', 'RoomController@edit')->name('edit_room');
Route::get('delete-room/{id}', 'RoomController@delete')->name('delete_room');
Route::post('update-room/{id}', 'RoomController@update')->name('update_room');

//Rent details
Route::get('rent/{id}', 'RentController@index')->name('rent');
Route::get('add-rent/{id}', 'RentController@add')->name('add_rent');
Route::post('store-rent/{id}', 'RentController@store')->name('store_rent');
Route::get('edit-rent/{id}', 'RentController@edit')->name('edit_rent');
Route::get('delete-rent/{id}', 'RentController@delete')->name('delete_rent');
Route::post('update-rent/{id}', 'RentController@update')->name('update_rent');

//Apartments
Route::get('add-apartment', 'ApartmentController@add')->name('add_apartment');
Route::post('store-apartment', 'ApartmentController@store')->name('store_apartment');
Route::get('feature-apartment/{id}', 'ApartmentController@feature')->name('feature_apartment');
Route::get('edit-apartment/{id}', 'ApartmentController@edit')->name('edit_apartment');
Route::get('delete-apartment/{id}', 'ApartmentController@delete')->name('delete_apartment');
Route::post('update-apartment/{id}', 'ApartmentController@update')->name('update_apartment');

//Apartments
Route::get('add-land', 'LandController@add')->name('add_land');
Route::post('store-land', 'LandController@store')->name('store_land');
Route::get('feature-land/{id}', 'LandController@feature')->name('feature_land');
Route::get('edit-land/{id}', 'LandController@edit')->name('edit_land');
Route::get('delete-land/{id}', 'LandController@delete')->name('delete_land');
Route::post('update-land/{id}', 'LandController@update')->name('update_land');

//Offices
Route::get('add-office', 'OfficeController@add')->name('add_office');
Route::post('store-office', 'OfficeController@store')->name('store_office');
Route::get('feature-office/{id}', 'OfficeController@feature')->name('feature_office');
Route::get('edit-office/{id}', 'OfficeController@edit')->name('edit_office');
Route::get('delete-office/{id}', 'OfficeController@delete')->name('delete_office');
Route::post('update-office/{id}', 'OfficeController@update')->name('update_office');

//Spaces
Route::get('add-space', 'SpaceController@add')->name('add_space');
Route::post('store-space', 'SpaceController@store')->name('store_space');
Route::get('feature-space/{id}', 'SpaceController@feature')->name('feature_space');
Route::get('edit-space/{id}', 'SpaceController@edit')->name('edit_space');
Route::get('delete-space/{id}', 'SpaceController@delete')->name('delete_space');
Route::post('update-space/{id}', 'SpaceController@update')->name('update_space');


//Inquiries

//Tenant inquiries
Route::get('tenant-contact-us', 'InquiryController@tenantContact')->name('inquiry');
Route::post('store-tenant-inquiry', 'InquiryController@store')->name('store_inquiry');
Route::get('view-tenant-inquiries', 'InquiryController@viewInquiries')->name('view_tenant_inquiries');

//Client inquiries
Route::post('store-client', 'ClientController@store')->name('store_client');


