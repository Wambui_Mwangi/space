<!DOCTYPE html>
<html>
  <head>
      <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet"> 
  </head>
  <body >
        <div class="header-content">
            <div class="inner">
                <p style="font-family: Montserrat">Hello {{$name}}, <br> You have successfully been registered as an agent on the Space and Occupy system. You can now view your tenants and property.</p>
                <p style="font-family: Montserrat">Your current password is: {{$password}} . Visit <a href="{{url('password/reset')}}">this link</a> to reset your password.</p>
                <p style="font-family: Montserrat">If you wish to login in immediately, click <a href="{{url('login')}}" >here.</a></p>
				        <p style="font-family: Montserrat">Regards, <br>Space and Occupy admin</p>                
            </div>
        </div>

  </body>
</html>