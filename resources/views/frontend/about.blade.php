@extends('frontend.layouts.app')

@section('content')
<!-- Page top section -->
	<section class="page-top-section set-bg" data-setbg="img/page-top-bg.jpg">
		<div class="container text-white">
			<h2>About us</h2>
		</div>
	</section>
	<!--  Page top end -->

	<!-- Breadcrumb -->
	<div class="site-breadcrumb">
		<div class="container">
			<a href=""><i class="fa fa-home"></i>Home</a>
			<span><i class="fa fa-angle-right"></i><b>About us</b></span>
		</div>
	</div>

	<!-- page -->
	<section class="page-section">
		<div class="container">
			<div class="row about-text">
				<div class=" ">
					<h5>ABOUT US</h5>
					<p>Space and Occupy Ventures is a company duly registered to deal with estates and property management and valuation consultancy and interior designs.<br>
					To ensure excellence with clients, the company employs best practices acts upon regular client feedback and bench marks that match the requirements.<br>
					Our professional team has specialized knowledge which is relevant to real time solutions.</p>
				</div>
				<div class="about-text">
					<h5>OUR CORE VALUES</h5>
					<ul class="about-list">
						<li><i class="fa fa-check-circle-o"></i>Sense of family.</li>
						<li><i class="fa fa-check-circle-o"></i>Unwavering integrity. </li>
						<li><i class="fa fa-check-circle-o"></i>Relentlessly innovative.</li>
						<li><i class="fa fa-check-circle-o"></i>Unquestioning reliability.</li>
						<li><i class="fa fa-check-circle-o"></i>Fervent belief in entrepreneurial success.</li>
					</ul>
					
				</div>
			</div>
				<div class="row about-text">
				<div class="">
					<h5>MISSION</h5>
					<p>To set high performance standards and manage properties for the success of our clients while ensuring our residents have best quality, most affordable and comforting living experience as possible.</p>
				</div>
				<div class="">
					<h5>VISION</h5>
					<p>To transform the property management through commitment to positive change and innovation that redefines consistency and quality expected by our clients and to become the property management company of choice that provides full service and care to our clients and team members.</p>
				</div>
			</div>
		</div>
		
@include('frontend.partials.team')
	</section>

@endsection