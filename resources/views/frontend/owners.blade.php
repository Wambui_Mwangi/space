@extends('frontend.layouts.app')
@section('content')
	<!-- Page top section -->
	<section class="page-top-section single-blog set-bg " data-setbg="img/page-top-bg.jpg">
		<div class="container text-white">
			<div class="row">
				<div class="col-xl-8 offset-lg-2">
					<h2>Information for property owners</h2>
					<!-- <a href=""><i class="fa fa-user"></i>Amanda Seyfried</a>
					<a href=""><i class="fa fa-clock-o"></i>25 Jun 2018</a> -->
				</div>
			</div>
		</div>
	</section>
	<!--  Page top end -->

	<!-- Breadcrumb -->
	<div class="site-breadcrumb">
		<div class="container">
			<a href=""><i class="fa fa-home"></i>Home</a>
			<span><i class="fa fa-angle-right"></i>Property owners</span>
		</div>
	</div>

	<!-- page -->
	<section class="page-section single-blog">
		<div class="container">
			<div class="row">
				<h4>We are excited to have you consider Space and Occupy Investments</h4>
				<div class=" singel-blog-content">
					<p>Space and Occupy provides our clients with management solutions that ensures their properties operate smoothly while increasing desirability and enhancing investment value. This has made property owners to depend on us as we help them in achieving their goals of property profitability and performance. Whether your property is residential homes, townhome, single family home, a condo or business premises, our company ensures it’s well managed to retain and attract great tenants.</p>
					<p>Our company prepares your property for rent, markets it through various channels, secures qualified tenants, protects your investment and also manages its cash flow. We also do partner with property owners or their realtors to ensure we maximize the cash flow and value your investment. Besides we provide various Special programs that protects and helps property owner’s life easier.</p>
					<blockquote>
						Our focus is to offer you the best services and results in the property management industry. We listen carefully so as to understand your real estate goals and then work hard to ensure we create solutions that make sense to you. It does not matter whether you are new to the market or an experienced investor because we have a tested and proven track record, the expertise and resources that can help you in achieving your goals.
					</blockquote>
					<p>Our services include: Asset management, Property management, Due diligence, Construction project management, Deal sourcing, Capital expense management, Underwriting, Custom leasing solutions, Custom account reporting, Strategic marketing and public relations, Transition planning and execution, Eco-friendly operational solutions, Value-add redevelopment, Staff training and online support. </p>
					<p>Having worked with many clients, here are some answers to <a href="{{route('owners_faqs')}}">frequently asked questions by property owners</a>.</p>
				</div>
				<div class="section-title">
							<h3>Get in touch</h3>
							<p>Send us an inquiry or let us know how we can partner with you.</p>
						</div>
						<form class="contact-form">
							<div class="row">
								<div class="col-md-6">
									<input type="text" placeholder="Your name">
								</div>
								<div class="col-md-6">
									<input type="text" placeholder="Your email">
								</div>
								<div class="col-md-12">
									<textarea  placeholder="Your message"></textarea>
									<button class="site-btn">SEND MESSAGE</button>
								</div>
							</div>
						</form>
					</div>
			</div>
		</div>
	</section>
	<!-- page end-->
@endsection