@extends('frontend.layouts.app')

@section('content')

	<!-- Page top section -->
	<section class="page-top-section set-bg" data-setbg="img/page-top-bg.jpg">
		<div class="container text-white">
			<h2>Talk to us</h2>
		</div>
	</section>
	<!--  Page top end -->

	<!-- Breadcrumb -->
	<div class="site-breadcrumb">
		<div class="container">
			<a href=""><i class="fa fa-home"></i>Home</a>
			<span><i class="fa fa-angle-right"></i>Contact us</span>
		</div>
	</div>

	<!-- page -->
	<section class="page-section blog-page">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<img src="img/space.png" alt="">
				</div>
				<div class="col-lg-6">
					<div class="contact-right">
						<div class="section-title">
							<h3>Get in touch</h3>
							<p>Send us an inquiry for clarification or more information</p>
						</div>
						@if(Session::has('success'))
                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="message" class="alert alert-success">
                                            {{ Session::get('success') }}
                                        </div>
                                    </div>
                                </div>
                            @endif
						<form class="contact-form" action="{{route('store_client')}}" method="post" enctype="multipart/form-data">
							{{ csrf_field() }}
							<div class="row">
								<div class="col-md-12">
									<input type="text" placeholder="Your name" name="name" value="{{ old('name') }}">
								</div>
								<div class="col-md-12">
									<input type="text" placeholder="Your email" name="email" value="{{ old('email') }}">
								</div>
								<div class="col-md-12">
									<input type="number" placeholder="Your phone number" name="telephone" value="{{ old('telephone') }}">
								</div>
								<div class="col-md-12">
									<textarea  placeholder="Your message" name="message" value="{{ old('message') }}"></textarea>
									<button class="site-btn" type="submit">SEND MESSAGE</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- page end -->
@endsection