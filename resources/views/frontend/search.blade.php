@extends('frontend.layouts.app')
@section('content')
	<!-- Page top section -->
	<section class="page-top-section set-bg" data-setbg="img/page-top-bg.jpg">
		<div class="container text-white">
			<h2>Search results</h2>
		</div>
	</section>
	<!--  Page top end -->

	<!-- Breadcrumb -->
	<div class="site-breadcrumb">
		<div class="container">
			<a href=""><i class="fa fa-home"></i>Home</a>
			<span><i class="fa fa-angle-right"></i>Search results</span>
		</div>
	</div>


	<!-- page -->
	<section class="page-section categories-page">
		<div class="container">
			<div class="row">
				@foreach($results as $result)
				@if($result->category_id == 2)
				<div class="col-lg-4 col-md-6">
					<!-- feature -->
					<div class="feature-item">
						<div class="feature-pic set-bg" data-setbg="img/{{$result->image}}">
							<div class="sale-notic">FOR SALE</div>
						</div>
						<div class="feature-text">
							<div class="text-center feature-title">
								<h5>{{$result->location}}</h5>
								<p><i class="fa fa-map-marker"></i> In {{$result->county->name}} County</p>
							</div>
							<div class="room-info-warp">
								<p>{{$result->description}}</p>
							</div>
							<a href="#" class="room-price"> Ksh {{$result->price}}</a>
						</div>
					</div>
				</div>
				@else
				<div class="col-lg-4 col-md-6">
					<!-- feature -->
					<div class="feature-item">
						<div class="feature-pic set-bg" data-setbg="img/{{$result->image}}">
							<div class="rent-notic">FOR Rent</div>
						</div>
						<div class="feature-text">
							<div class="text-center feature-title">
								<h5>{{$result->location}}</h5>
								<p><i class="fa fa-map-marker"></i> In {{$result->county->name}} County</p>
							</div>
							<div class="room-info-warp">
								<p>{{$result->description}}</p>
							</div>
							<a href="#" class="room-price"> Ksh {{$result->price}}/month</a>
						</div>
					</div>
				</div>
				@endif
				@endforeach
				
			</div>
			<div class="site-pagination">
				<span>1</span>
				<a href="#">2</a>
				<a href="#">3</a>
				<a href="#"><i class="fa fa-angle-right"></i></a>
			</div>
		</div>
	</section>
	<!-- page end -->
@endsection