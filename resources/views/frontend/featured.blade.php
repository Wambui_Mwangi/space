@extends('frontend.layouts.app')
@section('content')
	<!-- Page top section -->
	<section class="page-top-section set-bg" data-setbg="img/page-top-bg.jpg">
		<div class="container text-white">
			<h2>Properties</h2>
		</div>
	</section>
	<!--  Page top end -->

	<!-- Breadcrumb -->
	<div class="site-breadcrumb">
		<div class="container">
			<a href="{{route('view_properties')}}">Homes and houses</a>
			<a href="{{route('view_offices')}}">Offices</a>
			<a href="{{route('view_land')}}">Land</a>
			<a href="{{route('view_spaces')}}">Workspaces</a>
		</div>

	</div>

	@yield('properties-content')

	<!-- page -->
	

			
			</div>
			<div class="site-pagination">
				<span>1</span>
				<a href="#">2</a>
				<a href="#">3</a>
				<a href="#"><i class="fa fa-angle-right"></i></a>
			</div>
		</div>
	</section>
	<!-- page end -->
@endsection