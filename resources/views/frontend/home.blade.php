@extends('frontend.layouts.app')
@section('content')
	
<!-- Hero section -->
<section class="hero-section set-bg" data-setbg="img/bg.jpg">
	<div class="container hero-text text-white">
		<h2>We are your neighborhood professionals</h2>
		<p>Let us provide you with our excellent property  management services .<br>Find your dream property with us.</p>
		<a href="{{route('featured_properties')}}" class="site-btn">View available properties</a>
	</div>
</section>
<!-- Hero section end -->


@include('frontend.partials.search')

@if($properties)
@include('frontend.partials.properties')
@endif

@if($offices)
@include('frontend.partials.offices')
@endif

@include('frontend.partials.services')

@if($spaces)
@include('frontend.partials.spaces')
@endif

@if($lands)
@include('frontend.partials.lands')
@endif

@include('frontend.partials.featured')


	
@endsection