@extends('frontend.featured')
@section('properties-content')
<section class="page-section categories-page">
		<div class="container">
			<div class="row">
				@foreach ($offices as $office)
						@if($office->category_id == 2)
				<div class="col-lg-4 col-md-6">
					<!-- feature -->
					<div class="feature-item">
						<div class="feature-pic set-bg" data-setbg="img/{{$office->image}}">
							<div class="sale-notic">FOR SALE</div>
						</div>
						<div class="feature-text">
							<div class="text-center feature-title">
								<h5><i class="fa fa-map-marker"></i> {{$office->location}}</h5>
							</div>
							<div class="room-info-warp">
								<p>Size: {{$office->dimensions}}</p>
							</div>
							<a href="#" class="room-price"> Ksh {{$office->price}}</a>
						</div>
					</div>
				</div>
				@else
					<div class="col-lg-4 col-md-6">
					<!-- feature -->
					<div class="feature-item">
						<div class="feature-pic set-bg" data-setbg="img/{{$office->image}}">
							<div class="rent-notic">FOR Rent</div>
						</div>
						<div class="feature-text">
							<div class="text-center feature-title">
								<h5><i class="fa fa-map-marker"></i> {{$office->location}}</h5>
							</div>
							<div class="room-info-warp">
								<p>Size: {{$office->dimensions}}</p>
							</div>
							<a href="#" class="room-price"> Ksh {{$office->price}}/month</a>
						</div>
					</div>
				</div>
				@endif
			@endforeach
@endsection