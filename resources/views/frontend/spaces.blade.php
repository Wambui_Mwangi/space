@extends('frontend.featured')
@section('properties-content')
<section class="page-section categories-page">
		<div class="container">
			<div class="row">
				@foreach ($spaces as $space)
				<div class="col-lg-4 col-md-6">
					<!-- feature -->
					<div class="feature-item">
						<div class="feature-pic set-bg" data-setbg="img/{{$space->image}}">
						</div>
						<div class="feature-text">
							<div class="text-center feature-title">
								<h5><i class="fa fa-map-marker"></i> {{$space->location}}</h5>
							</div>
							<div class="room-info-warp">
								<p>Size: {{$space->dimensions}}</p>
							</div>
							<a class="room-price"> Ksh {{$space->rates->first()->amount}} for {{$space->rates->first()->duration}}</a>
						</div>
					</div>
				</div>
					
			@endforeach
@endsection