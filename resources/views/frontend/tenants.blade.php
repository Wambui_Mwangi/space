@extends('frontend.layouts.app')
@section('content')
	<!-- Page top section -->
	<section class="page-top-section single-blog set-bg " data-setbg="img/page-top-bg.jpg">
		<div class="container text-white">
			<div class="row">
				<div class="col-xl-8 offset-lg-2">
					<h2>Information for tenants</h2>
					<!-- <a href=""><i class="fa fa-user"></i>Amanda Seyfried</a>
					<a href=""><i class="fa fa-clock-o"></i>25 Jun 2018</a> -->
				</div>
			</div>
		</div>
	</section>
	<!--  Page top end -->

	<!-- Breadcrumb -->
	<div class="site-breadcrumb">
		<div class="container">
			<a href=""><i class="fa fa-home"></i>Home</a>
			<span><i class="fa fa-angle-right"></i>Tenants</span>
		</div>
	</div>

	<!-- page -->
	<section class="page-section single-blog">
		<div class="container">
			<div class="row">
				<h4>We are excited to have you consider Space and Occupy Investments</h4>
				<div class=" singel-blog-content">
					<p>Space and Occupy Investments is tour one stop shop for all your property needs. Not only do we have diverse properties for you to consider, we have thorough checked them to ensure that your place of residence will be a place you are happy to call home.</p>
					<p>We provide the direct link between you and the porperty owner and effectively deal with any issues you may have in your place of residence without unnecessary bureaucracy.</p>
					<blockquote>
						Our company culture drives us to focus on the people. We believe that we not only serve the community but are part of the community that we serve.
					</blockquote>
					<p>We look out for the tenant in many ways, part of which is making the process of moving in and out very hasssle free and easy. In addition, we have a Focus green initiative that: aims at reducing costs, incorporates energy efficient lighting appliances that add value to the contemporary renters for healthier and utility cost saving homes, uses paints with zero VOC colorants that allows improvement of indoor air quality thus providing a cleaner environment for our residents.</p>

					<p>Having worked with many clients, here are some answers to <a href="{{route('tenants_faqs')}}">frequently asked questions by tenants</a>.</p>
				</div>
				<div class="section-title">
							<h3>Get in touch</h3>
							<p>Send us an inquiry for clarification or more information</p>
						</div>
						<form class="contact-form">
							<div class="row">
								<div class="col-md-6">
									<input type="text" placeholder="Your name">
								</div>
								<div class="col-md-6">
									<input type="text" placeholder="Your email">
								</div>
								<div class="col-md-12">
									<textarea  placeholder="Your message"></textarea>
									<button class="site-btn">SEND MESSAGE</button>
								</div>
							</div>
						</form>
					</div>
			</div>
		</div>
	</section>
	<!-- page end-->
@endsection