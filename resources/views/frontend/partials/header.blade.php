<!-- Header section -->
<header class="header-section">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="site-navbar">
					<a href="#" class="site-logo"><img src="img/space-small.png" alt=""></a>
					<div class="nav-switch">
						<i class="fa fa-bars"></i>
					</div>
					<ul class="main-menu">
						<li><a href="{{route('welcome')}}">Home</a></li>

						<li><a href="{{route('view_properties')}}" >Property</a></li>
						<li><a href="{{route('about_us')}}">About us</a></li>
						<li><a href="{{route('services')}}">Services</a></li>
						<li><a href="{{route('tenants_information')}}">Tenants Info</a></li>
						<li><a href="{{route('owners_information')}}">Owners Info</a></li>
						<li><a href="{{route('contact_us')}}">Contact us</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</header>
<!-- Header section end -->
