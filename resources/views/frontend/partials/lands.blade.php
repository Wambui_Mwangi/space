<!-- Properties section -->
	<section class="properties-section spad">
		<div class="container">
			<div class="section-title text-center">
				<h3>lands</h3>
			</div>
			<div class="row">
				@foreach($lands as $land)
					<div class="col-md-6">
						<div class="propertie-item set-bg" data-setbg="img/{{$land->image}}">
							<div class="sale-notic">FOR SALE</div>
							<div class="propertie-info text-white">
								<div class="info-warp">
									<h5>{{$land->location}}</h5>
									<p><i class="fa fa-map-marker"></i> {{$land->pin}}</p>
								</div>
								<div class="price">Ksh {{$land->lower_price}} - {{$land->upper_price}}</div>
							</div>
						</div>
					</div>
				@endforeach
			</div>
			<button class="site-btn centered">View all lands</button>
		</div>
	</section>
	<!-- Properties section end -->