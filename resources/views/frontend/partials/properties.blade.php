<!-- Properties section -->
	<section class="properties-section spad">
		<div class="container">
			<div class="section-title text-center">
				<h3>HOMES AND HOUSES</h3>
			</div>
			<div class="row">
				@foreach($properties as $property)
				@if($property->category_id == 2)
				<div class="col-md-6">
					<div class="propertie-item set-bg" data-setbg="img/{{$property->image}}">
						<div class="sale-notic">FOR SALE</div>
						<div class="propertie-info text-white">
							<div class="info-warp">
								<h5>{{$property->location}}</h5>
								<p><i class="fa fa-map-marker"></i> In {{$property->county->name}} county</p>
							</div>
							<div class="price">Ksh {{$property->price}}</div>
						</div>
					</div>
				</div>
				@else
				<div class="col-md-6">
					<div class="propertie-item set-bg" data-setbg="img/{{$property->image}}">
						<div class="rent-notic">FOR RENT</div>
						<div class="propertie-info text-white">
							<div class="info-warp">
								<a href="">
								<h5>{{$property->location}}</h5>
								<p><i class="fa fa-map-marker"></i> In {{$property->county->name}} county</p>
								</a>
							</div>
							<a href="">
							<div class="price">Ksh {{$property->price}}/month</div>
							</a>
						</div>
					</div>
				</div>
				@endif
				@endforeach
			</div>
			<button class="site-btn text-center section-title">View all homes and houses</button>
		</div>
	</section>
	<!-- Properties section end -->