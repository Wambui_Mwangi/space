<!-- Filter form section -->
	<div class="filter-search">
		<div class="container">
			<form class="filter-form" action="{{ route('search')}}" method="post">
				{{ csrf_field() }}
				<input type="text" placeholder="Enter your specifications here" name="specifications">
				
				<button class="site-btn fs-submit" type="submit">SEARCH</button>
			</form>
		</div>
	</div>
	<!-- Filter form section end -->
