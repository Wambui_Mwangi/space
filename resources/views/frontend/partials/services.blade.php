<!-- Services section -->
	<section class="services-section spad set-bg" data-setbg="img/service-bg.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<img src="img/services1.jpg" alt="">
				</div>
				<div class="col-lg-5 offset-lg-1 pl-lg-0">
					<div class="section-title text-white">
						<h3>OUR SERVICES</h3>
						<p>We offer the following services: </p>
					</div>
					<div class="services">
						<div class="service-item">
							<i class="fa fa-comments"></i>
							<div class="service-text">
								<h5>Consultancy Services</h5>
								<p>We offer consultancy services on asset valuation, asset management and various real estate investments to our property owners.</p>
							</div>
						</div>
						<div class="service-item">
							<i class="fa fa-home"></i>
							<div class="service-text">
								<h5>Properties Management</h5>
								<p>We manage the buying and selling of properties by connecting owners with clients.</p>
							</div>
						</div>
						<div class="service-item">
							<i class="fa fa-briefcase"></i>
							<div class="service-text">
								<h5>Renting and Leasing</h5>
								<p>We facilitate the renting and leasing of buildings through rent collection, repairs on buildings and administrative overhead.</p>
							</div>
						</div>
						<div class="service-item">
							<i class="fa fa-building"></i>
							<div class="service-text">
								<h5>Interior Design</h5>
								<p>We conduct interior design for all our cliens to ensure that your property is a place you can call home.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Services section end -->