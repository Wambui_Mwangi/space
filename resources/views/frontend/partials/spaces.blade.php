<!-- Properties section -->
	<section class="properties-section spad">
		<div class="container">
			<div class="section-title text-center">
				<h3>spaces</h3>
			</div>
			<div class="row">
				@foreach($spaces as $space)
					<div class="col-md-6">
						<div class="propertie-item set-bg" data-setbg="img/{{$space->image}}">
							<div class="rent-notic">FOR RENT</div>
							<div class="propertie-info text-white">
								<div class="info-warp">
									<h5>{{$space->location}}</h5>
									<p><i class="fa fa-map-marker"></i> {{$space->pin}}</p>
								</div>
								<div class="price">Ksh {{$space->price}}/month</div>
							</div>
						</div>
					</div>
				@endforeach
			</div>
			<button class="site-btn centered">View all spaces</button>
		</div>
	</section>
	<!-- Properties section end -->