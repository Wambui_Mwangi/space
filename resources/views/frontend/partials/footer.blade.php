<!-- Footer section --> 
	<footer class="footer-section set-bg" data-setbg="img/footer-bg.jpg">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6 footer-widget">
					<img src="img/space-small.png" alt="">
					<p>Space and Occupy Investments is your one stop shop for all your property needs.</p>
					<div class="social">
						<a href="https://www.facebook.com/SPACE-and-Occupy-Investments-1082540708489474/"><i class="fa fa-facebook"></i></a>
						<a href="#"><i class="fa fa-twitter"></i></a>
						<a href="#"><i class="fa fa-instagram"></i></a>
						<a href="#"><i class="fa fa-pinterest"></i></a>
						<a href="#"><i class="fa fa-linkedin"></i></a>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 footer-widget">
					<div class="contact-widget">
						<h5 class="fw-title">CONTACT US</h5>
						<p><i class="fa fa-map-marker"></i>2nd floor, Taj Mall along Eastern Bypass </p>
						<p><i class="fa fa-phone"></i>020-8070084 / 020-7602949</p>
						<p><i class="fa fa-envelope"></i>info@spaceoccupy.co.ke</p>
						<p><i class="fa fa-clock-o"></i>Mon - Sat, 08 AM - 05 PM</p>
					</div>
				</div>
				
			</div>
			<div class="footer-bottom">
				<div class="footer-nav">
					<ul class="">
						<li><a href="{{route('welcome')}}">Home</a></li>
						<li><a href="{{route('featured_properties')}}">Featured property</a></li>
						<li><a href="{{route('about_us')}}">About us</a></li>
						<li><a href="{{route('tenants_information')}}">Tenants Info</a></li>
						<li><a href="{{route('owners_information')}}">Owners Info</a></li>
						<li><a href="{{route('contact_us')}}">Contact us</a></li>
					</ul>
				</div>
				<div class="copyright">
					<p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
<!-- Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a> -->
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
				</div>
			</div>
		</div>
	</footer>
	<!-- Footer section end