<!-- Properties section -->
	<section class="properties-section spad">
		<div class="container">
			<div class="section-title text-center">
				<h3>OFFICES</h3>
			</div>
			<div class="row">
				@foreach($offices as $office)
					@if($office->category_id == 2)
					<div class="col-md-6">
						<div class="propertie-item set-bg" data-setbg="img/{{$office->image}}">
							<div class="sale-notic">FOR SALE</div>
							<div class="propertie-info text-white">
								<div class="info-warp">
									<h5>{{$office->location}}</h5>
									<p><i class="fa fa-map-marker"></i> {{$office->pin}}</p>
								</div>
								<div class="price">Ksh {{$office->price}}</div>
							</div>
						</div>
					</div>
					@else
					<div class="col-md-6">
						<div class="propertie-item set-bg" data-setbg="img/{{$office->image}}">
							<div class="rent-notic">FOR RENT</div>
							<div class="propertie-info text-white">
								<div class="info-warp">
									<h5>{{$office->location}}</h5>
									<p><i class="fa fa-map-marker"></i> {{$office->pin}}</p>
								</div>
								<div class="price">Ksh {{$office->price}}/month</div>
							</div>
						</div>
					</div>
					@endif
				@endforeach
			</div>
			<button class="site-btn centered">View all offices</button>
		</div>
	</section>
	<!-- Properties section end -->