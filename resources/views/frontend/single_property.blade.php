@extends('frontend.layouts.app')

@section('content')
<!-- Page top section -->
	<section class="page-top-section set-bg" data-setbg="img/page-top-bg.jpg">
		<div class="container text-white">
			<h2>What we do</h2>
		</div>
	</section>
	<!--  Page top end -->

	<!-- Breadcrumb -->
	<div class="site-breadcrumb">
		<div class="container">
			<a href=""><i class="fa fa-home"></i>Home</a>
			<span><i class="fa fa-angle-right"></i><b>Our services</b></span>
		</div>
	</div>

	<!-- page -->
	<section class="page-section">
		<div class="container">
			<div class="row about-text text-center">
				<div class=" ">
					<h5>Consultancy </h5>
					<p>Space and Occupy Ventures offers consultancy services in the areas of: acquisition, development, construction, underwriting, note purchase, REO purchase, bankruptcy trustee sales, fee services and recapitalization among many  more. It easy to lose money in investments gone wrong so we help you do the research beforehand.</p>
				</div>
				<div class=" ">
					<h5>Property Management </h5>
					<p>To home owners and investors looking for companies with experts in property management, Space and Occupy provides specialized and personal attention that your property and you deserve. This encompasses; providing dependable and qualified tenants, ensuring we get your rent on time, keeping your property at all time in excellent condition and we’ve also initiated an “Owner’s Hotline” for exclusive inquiries by owners.</p>
				</div>
				<div class=" ">
					<h5>Renting, Leasing and selling </h5>
					<p>To home buyers, Space and Occupy Company offers an unprecedented service that enables one to find property that fits well with their need. Whether being a first time buyer or a seasoned buyer we help in ensuring you avoid purchasing pitfalls thus leading you to a satisfying and successful closing.</p>
					<p>
					In case you’re selling a property, our company has an inventory of potential buyers as well as a marketing program that extensive for sellers. The program includes; web site, local advertising, virtual tours, signage and talking homes, brochures and flyers.</p>
					<p>
					For tenants seeking perfect place to call home, we have it. More importantly, our services do not end with signing of a lease agreement but that marks our journey together with continued communication plus emergency numbers and services. We walk together through the entire lease period to make sure your rental experience is rewarding and pleasant.
					</p>
				</div>
				<div class=" ">
					<h5>Interior Design </h5>
					<div class="row">
						<div class="col-lg-6 col-md-6">
							<img src="{{asset('img/interior2.jpeg')}}">
						</div>
						<div class="col-lg-6 col-md-6">
							<img src="{{asset('img/interior6.jpeg')}}">
						</div>
						
					</div>
					<p><br>
					Space and Occupy will spruce up your home or office and make it a place forward to going to. Whether its a brand new building building or you're revamping, give us a call and we'll give you a brand new look.</p>
				</div>
			</div>
				
		</div>
		
	</section>

@endsection