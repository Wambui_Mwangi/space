@extends('frontend.featured')
@section('properties-content')
<section class="page-section categories-page">
		<div class="container">
			<div class="row">
				@foreach ($lands as $land)
				<div class="col-lg-4 col-md-6">
					<!-- feature -->
					<div class="feature-item">
						<div class="feature-pic set-bg" data-setbg="img/{{$land->image}}">
						</div>
						<div class="feature-text">
							<div class="text-center feature-title">
								<h5><i class="fa fa-map-marker"></i> {{$land->location}}</h5>
							</div>
							<div class="room-info-warp">
								<p>Size: {{$land->dimensions}}</p>
							</div>
							<a href="#" class="room-price"> Ksh {{$land->lower_price}} - Ksh {{$land->upper_price}}</a>
						</div>
					</div>
				</div>
					
			@endforeach
@endsection