@extends('frontend.featured')
@section('properties-content')
<section class="page-section categories-page">
		<div class="container">
			<div class="row">
				@foreach ($properties as $property)
						@if($property->category_id == 2)
				<div class="col-lg-4 col-md-6">
					<!-- feature -->
					<div class="feature-item">
						<div class="feature-pic set-bg" data-setbg="img/{{$property->image}}">
							<div class="sale-notic">FOR SALE</div>
						</div>
						<div class="feature-text">
							<div class="text-center feature-title">
								<h5>{{$property->location}}</h5>
								<p><i class="fa fa-map-marker"></i> In {{$property->county->name}} County</p>
							</div>
							<div class="room-info-warp">
								<p>{{$property->description}}</p>
							</div>
							<a href="#" class="room-price"> Ksh {{$property->price}}</a>
						</div>
					</div>
				</div>
				@else
					<div class="col-lg-4 col-md-6">
					<!-- feature -->
					<div class="feature-item">
						<div class="feature-pic set-bg" data-setbg="img/{{$property->image}}">
							<div class="rent-notic">FOR Rent</div>
						</div>
						<div class="feature-text">
							<div class="text-center feature-title">
								<h5>{{$property->location}}</h5>
								<p><i class="fa fa-map-marker"></i> In {{$property->county->name}} County</p>
							</div>
							<div class="room-info-warp">
								<p>{{$property->description}}</p>
							</div>
							<a class="room-price"> Ksh {{$property->price}}/month</a>
						</div>
					</div>
				</div>
				@endif
			@endforeach
@endsection