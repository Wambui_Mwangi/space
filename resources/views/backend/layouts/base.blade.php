<!DOCTYPE html>
<html lang="en"> 
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Space and Occupy</title>
    <link rel="stylesheet" href="{{ URL::to('css/main.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
    <link rel="icon" href="{{asset('img/space-logo.png')}}">
    <link rel="stylesheet" href="{{ URL::to('css/base.css') }}">
@yield('styles')

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="app sidebar-mini rtl">
    @include('backend.partials.header')
    @include('backend.partials.sidenavbar')
    @yield('content')
<script type="text/javascript" src="{{ URL::to('js/jquery-3.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('js/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('js/backend-main.js') }}"></script>
<script type="text/javascript" src="{{ URL::to('js/pace.min.js') }}"></script>
@yield('scripts')
</body>
</html>