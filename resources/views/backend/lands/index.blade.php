@extends('backend.layouts.base')

@section('content')
    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fa fa-users"></i> Land</h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('add_land') }}">Add land</a></li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="row">
                        <div class="col-md-12">
                            <div id="message" class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        </div>
                    </div>
                @endif
        @foreach($lands->chunk(2) as $chunk)
            <div class="row">
                @foreach($chunk as $land)
                    <div class="col-md-6">
                        <div class="tile">
                            <div class="tile-title-w-btn">
                                <h3 class="title">
                                    <img src="{{asset('img/'. $land->image)}}" class="img-responsive" width="90px" height="90px" />
                                </h3>
                                <div class="btn-group">
                                    {{ $land->location }}
                                </div>
                            </div>
                            <div class="tile-body">
                                <p>{{ $land->details }}</p>
                                <div class="tile-title-w-btn">
                                    <p>Size of land: {{$land->dimensions}}</p>
                                    
                                </div>
                                <p>Price range: Ksh {{$land->lower_price}} - Ksh {{$land->upper_price}}</p>
                                
                                <p>Google pin: {{$land->pin}}</p>


                            </div>
                            <div class="tile-footer">
                                <div class="btn-group">
                                    <a class="btn btn-primary" href="{{ route('edit_land', ['id' => $land->id]) }}"><i class="fa fa-lg fa-edit"></i></a>
                                    <a class="btn btn-primary" href="{{ route('delete_land', ['id' => $land->id]) }}"><i class="fa fa-lg fa-trash"></i></a>
                                        
                                    
                                </div>
                            </div>

                        </div>
                    </div>
                @endforeach
            </div>
        @endforeach

            </div>
        </div>
    </main>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ URL::to('js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::to('js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
@endsection
