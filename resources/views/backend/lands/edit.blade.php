@extends('backend.layouts.base')

@section('content')
    <main class="app-content">
        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <h3 class="tile-title">Update land</h3>
                    <hr>
                    <div class="tile-body">
                        <div class="col-md-12">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{route('update_land', ['id' => $land->id])}} " method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <p>Select land image</p>
                                <div class="custom-file">

                                  <input type="file" class="custom-file-input" id="customFile" name="image">
                                  <label class="custom-file-label" for="customFile">Choose land image</label>
                                </div>
                                <div class="form-group">
                                <div class="form-group">
                                    <label class="control-label">Land location</label>
                                    <input class="form-control"  type="text" name="location" value="{{ $land->location }}">
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                    <label class="control-label">Google Pin</label>
                                    <input class="form-control" type="text" name="pin" value="{{ $land->pin }}">
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                    <label class="control-label">Land Size</label>
                                    <input class="form-control"  type="text" name="dimensions" value="{{ $land->dimensions }}">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Lower Price</label>
                                    <input class="form-control" type="number" name="lower_price" value="{{ $land->lower_price }}">
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Upper Price</label>
                                    <input class="form-control"  type="number" name="upper_price" value="{{ $land->upper_price }}">
                                </div>

                                </div>
                                
                                <br><br>
                                <div class="">
                                    <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update land</button></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('scripts')
<script type="application/javascript">
    $('input[type="file"]').change(function(e){
        var fileName = e.target.files[0].name;
        $('.custom-file-label').html(fileName);
    });
</script>
<script type="text/javascript">
    $(function() {
  $("#addMore").click(function(e) {
    e.preventDefault();
    $("#fieldList:first").clone().appendTo("#new");
  });
});
</script>
@endsection