@extends('backend.layouts.base') 

@section('content')
<main class="app-content">
    <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <h3 class="tile-title">Edit user's information</h3>
                    <hr>
                    <div class="tile-body">
                        <div class="col-md-12">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{ route('update_user', ['id' => $user->id])}}" method="post">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label class="control-label">Enter the user's name</label>
                                    <input class="form-control" value="{{ $user->name }}"  type="text" name="name">
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Enter the user's email</label>
                                    <input class="form-control" value="{{ $user->email }}"  type="email" name="email">
                                </div>


                                <div class="form-group">
                                    <label class="control-label">Enter the user's phone number</label>
                                    <input class="form-control" value="{{ $user->telephone}}"  type="number" name="telephone">
                                </div>

                                <div class="form-group">
                                    <label class="control-label"> Select the user's role</label>
                                    <select class="form-control" name="role">
                                    <option value="{{$user->role}}">{{$user->role}}</option>
                                    <option value="agent">Agent</option>
                                    <option value="owner">Owner</option>
                                    <option value="tenant">Tenant</option>
                                    </select>
                                </div>
                                

                                <div class="">
                                    <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update user details</button></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</main>
@endsection


