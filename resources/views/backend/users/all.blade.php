@extends('backend.users.index')

@section('users-content')        
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="row">
                        <div class="col-md-12">
                            <div id="message" class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        </div>
                    </div>
                @endif
                    <div class="table-responsive">
                        <table class="table  table-hover table-bordered" id="sampleTable">
                            <thead class="thead-light">
                            <tr>
                                <th>Name</th>
                                <th>Telephone</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Deactivate user</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($users as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->telephone }}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->role}}</td>
                                    <td>
                                    @if($user->is_active == 0)
                                    Deactivated<br>
                                    <a href="{{route('deactivate_user', ['id' => $user->id])}}">Activate user</a>
                                    @else
                                    Active<br>
                                    <a href="{{route('deactivate_user', ['id' => $user->id])}}">Deactivate user</a>
                                    @endif</td>
                                </tr>
                            @empty
                                <p>No properties yet!</p>
                            @endforelse
                            </tbody>
                        </table>
                </div>
            </div>
        </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ URL::to('js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::to('js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
@endsection
