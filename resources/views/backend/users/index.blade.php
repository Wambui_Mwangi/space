@extends('backend.layouts.base')

@section('content')
	<main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fa fa-users"></i>Users</h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('agents')}} ">Agents</a></li>
            </ul>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('owners')}}">Owners</a></li>
            </ul>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('tenants')}}">Tenants</a></li>
            </ul>
            @if(Auth::user()->role=='admin')
            
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('users')}}">All users</a></li>
            </ul>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('add_user')}}">Add a user</a></li>
            </ul>
            @endif
           
            
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="row">
                        <div class="col-md-12">
                            <div id="message" class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        </div>
                    </div>
                @endif
                <div class="tile">
                    {{--<!-- <h3 class="tile-title">users</h3> --> --}}
                    @yield('users-content')
            </div>
        </div>
    </main>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready( function () {
    $('#myTable').DataTable();
} );
</script>
@endsection