@extends('backend.users.index')

@section('users-content')
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="row">
                        <div class="col-md-12">
                            <div id="message" class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        </div>
                    </div>
                @endif
                    <div class="table-responsive">
                        <table class="table  table-bordered table-hover" id="sampleTable">
                            <thead class="thead-light">
                            <tr>
                                <th>Name</th>
                                <th>Telephone</th>
                                <th>Email</th>
                                <th>Edit agent details</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($agents as $agent)
                                <tr>
                                    <td>{{ $agent->name }}</td>
                                    <td>{{ $agent->telephone }}</td>
                                    <td>{{$agent->email}}</td>
                                    <td><a href="{{ route('edit_agent', ['id' => $agent->id]) }}">Edit agent details</a></td>
                                    
                                </tr>
                            @empty
                                <p>No agents yet!</p>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ URL::to('js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::to('js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
@endsection
