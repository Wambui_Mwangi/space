@extends('backend.users.index')

@section('users-content')
    <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <h3 class="tile-title">Add an agent</h3>
                    <hr>
                    <div class="tile-body">
                        <div class="col-md-12">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{ route('store_agent')}}" method="post">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label class="control-label">Enter the agent's name</label>
                                    <input class="form-control" value="{{ old('name') }}"  type="text" name="name">
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Enter the agent's email</label>
                                    <input class="form-control" value="{{ old('email') }}"  type="email" name="email">
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Enter the agent's initial password</label>
                                    <input class="form-control" value="{{ old('password') }}"  type="text" name="password">
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Enter the agent's phone number</label>
                                    <input class="form-control" value="{{ old('telephone') }}"  type="number" name="telephone">
                                </div>

                                <div class="form-group" id="cuList">
                                    <label class="control-label">Add the agent's properties</label>
                                    <select class="form-control" name="property_id[]" id="cu" multiple="multiple">
                                    <option value=" ">Select option</option>
                                    @foreach($properties as $property)
                                        <option value="{{$property->id}}">{{$property->name}}</option>
                                    @endforeach
                                    </select>
                                    </select>

                                </div>
                                

                                <div class="">
                                    <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Add the agent</button></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection


@section('scripts')
<script type="text/javascript">
    $(function() {
  $("#addMore").click(function(e) {
    e.preventDefault();
    $("#cuList:last").clone().prependTo("#new");
  });
});
</script>

<script type="text/javascript">
    $(document).ready(function ()
    {
            $('#region').on('change',function(){
               var regionID = $(this).val();
               if(regionID)
               {
                  $.ajax({
                     url : 'getcus/' +regionID,
                     type : "GET",
                     dataType : "json",
                     success:function(data)
                     {
                        console.log(data);
                        $('#cu').empty();
                        $.each(data, function(key,value){
                           $('#cu').append('<option value="'+ key +'">'+ value +'</option>');
                        });
                     }
                  });
               }
               else
               {
                  $('#cu').empty();
               }
            });
    });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script type="text/javascript">
      $('#cu').select2();
    </script>
@endsection