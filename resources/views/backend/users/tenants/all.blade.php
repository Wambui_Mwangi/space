@extends('backend.users.index')

@section('users-content')
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="row">
                        <div class="col-md-12">
                            <div id="message" class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        </div>
                    </div>
                @endif
                    
                    <div class="table-responsive">
                        <table class="table  table-hover table-bordered" id="sampleTable">
                            <thead class="thead-light">
                            <tr>
                                <th>Name</th>
                                <th>Telephone</th>
                                <th>Email</th>
                                <th>Edit tenant details</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($tenants as $tenant)
                                <tr>
                                    <td>{{ $tenant->name }}</td>
                                    <td>{{ $tenant->telephone }}</td>
                                    <td>{{$tenant->email}}</td>
                                    <td><a href="{{ route('edit_tenant', ['id' => $tenant->id]) }}">Edit tenant details</a></td>
                                    
                                </tr>
                            @empty
                                <p>No tenants yet!</p>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ URL::to('js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::to('js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
@endsection
