@extends('backend.layouts.base') 

@section('content')
<main class="app-content">
    <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <h3 class="tile-title">Add a tenant</h3>
                    <hr>
                    <div class="tile-body">
                        <div class="col-md-12">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{ route('store_tenant')}}" method="post">
                                {{ csrf_field() }}

                                <div class="form-group">
                                    <label class="control-label">Enter the tenant's name</label>
                                    <input class="form-control" value="{{ old('name') }}"  type="text" name="name">
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Enter the tenant's email</label>
                                    <input class="form-control" value="{{ old('email') }}"  type="email" name="email">
                                </div>


                                <div class="form-group">
                                    <label class="control-label">Enter the user's phone number</label>
                                    <input class="form-control" value="{{ old('telephone') }}"  type="number" name="telephone">
                                </div>

                                <div class="form-group">
                                    <label class="control-label"> Select the tenant's current residence</label>
                                    <select class="form-control" name="property_id">
                                    <option value=" ">Select option</option>
                                    @foreach($properties as $property)
                                        <option value="{{$property->id}}">{{$property->name}}</option>
                                    @endforeach
                                    </select>
                                </div>
                                

                                <div class="">
                                    <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Add the user</button></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</main>
@endsection


