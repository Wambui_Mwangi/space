@extends('backend.users.index')

@section('users-content')
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="row">
                        <div class="col-md-12">
                            <div id="message" class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        </div>
                    </div>
                @endif
                    <div class="table-responsive">
                        <table class="table  table-bordered table-hover" id="sampleTable">
                            <thead class="thead-light">
                            <tr>
                                <th>Name</th>
                                <th>Telephone</th>
                                <th>Email</th>
                                <th>Edit owner details</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($owners as $owner)
                                <tr>
                                    <td>{{ $owner->name }}</td>
                                    <td>{{ $owner->telephone }}</td>
                                    <td>{{$owner->email}}</td>
                                    <td><a href="{{ route('edit_user', ['id' => $owner->id]) }}">Edit owner details</a></td>
                                    
                                </tr>
                            @empty
                                <p>No owners yet!</p>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ URL::to('js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::to('js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
@endsection
