@extends('backend.layouts.base')

@section('content')
    <main class="app-content">
        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <h3 class="tile-title">Add office</h3>
                    <hr>
                    <div class="tile-body">
                        <div class="col-md-12">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{route('store_office')}} " method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <p>Select office image</p>
                                <div class="custom-file">

                                  <input type="file" class="custom-file-input" id="customFile" name="image">
                                  <label class="custom-file-label" for="customFile">Choose office image</label>
                                </div>
                                <div class="form-group">
                                <div class="form-group">
                                    <label class="control-label"> Office Category</label>
                                    <select class="form-control" type="number" name="category_id" value="{{ old('category_id') }}">
                                        <option value=" ">None</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Office location</label>
                                    <input class="form-control" placeholder="Enter the location of the office" type="text" name="location" value="{{ old('location') }}">
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                    <label class="control-label">Google Pin</label>
                                    <input class="form-control" placeholder="Copy and paste the office's Google Maps pin here" type="text" name="pin" value="{{ old('pin') }}">
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                    <label class="control-label">Office Size</label>
                                    <input class="form-control" placeholder="Enter the size  of the office eg 80 x 100" type="text" name="dimensions" value="{{ old('dimensions') }}">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Price</label>
                                    <input class="form-control" placeholder="Enter the price  for this office" type="number" name="price" value="{{ old('price') }}">
                                </div>

                                </div>
                                
                                <br><br>
                                <div class="">
                                    <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Add office</button></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('scripts')
<script type="application/javascript">
    $('input[type="file"]').change(function(e){
        var fileName = e.target.files[0].name;
        $('.custom-file-label').html(fileName);
    });
</script>
<script type="text/javascript">
    $(function() {
  $("#addMore").click(function(e) {
    e.preventDefault();
    $("#fieldList:first").clone().appendTo("#new");
  });
});
</script>
@endsection