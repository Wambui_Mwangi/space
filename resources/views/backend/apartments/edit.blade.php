@extends('backend.layouts.base') 

@section('content')
    <main class="app-content">
        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <h3 class="tile-title">Update apartment information</h3>
                    <hr>
                    <div class="tile-body">
                        <div class="col-md-12">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{route('update_apartment', ['id' => $apartment->id])}} " method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                <p>Select apartment image</p>
                                <div class="custom-file">

                                  <input type="file" class="custom-file-input" id="customFile" name="image">
                                  <label class="custom-file-label" for="customFile">Choose apartment image 
                                    <span class="custom-file-control form-control-file"></span>
                                </label>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Apartment location</label>
                                    <input class="form-control" type="text" name="location" value="{{ $apartment->location }}">
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                    <label class="control-label">Google Pin</label>
                                    <input class="form-control" type="text" name="pin" value="{{ $apartment->pin }}">
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                    <label class="control-label">Apartment Type</label>
                                    <input class="form-control"  type="text" name="units" value="{{ $apartment->units }}">
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Description</label>
                                    <input class="form-control" type="integer" name="details" value="{{ $apartment->details }}">
                                </div>
                                <h5>Rates</h5>
                                @foreach($rates as $rate)
                                <div class="form-group" id="fieldList">
                                    <label class="control-label">Charges</label>
                                    <input class="form-control" type="number" name="amount[]" value="{{ $rate->amount }}">

                                    <label class="control-label">Duration</label>
                                    <input class="form-control" placeholder="Enter the duration for the rate eg 1 week, 1 day " type="text" name="duration[]" value="{{ $rate->duration }}">
                                </div>
                                @endforeach
                                <div id="new"></div>

                                <div class=" ">
                                    <button class="btn btn-warning" id="addMore">Add more rates</button>
                                    
                                </div>
                                </div>
                                
                                
                                
                                <br><br>
                                <div class="">
                                    <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update apartment</button></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('scripts')
<script type="application/javascript">
    $('input[type="file"]').change(function(e){
        var fileName = e.target.files[0].name;
        $('.custom-file-label').html(fileName);
    });
</script>
<script type="text/javascript">
    $(function() {
  $("#addMore").click(function(e) {
    e.preventDefault();
    $("#fieldList:first").clone().appendTo("#new");
  });
});
</script>
@endsection