@extends('backend.layouts.base')

@section('content')
    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fa fa-users"></i> Apartments</h1>
            </div>
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('add_apartment') }}">Add an apartment</a></li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="row">
                        <div class="col-md-12">
                            <div id="message" class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        </div>
                    </div>
                @endif
        @foreach($apartments->chunk(2) as $chunk)
            <div class="row">
                @foreach($chunk as $apartment)
                    <div class="col-md-6">
                        <div class="tile">
                            <div class="tile-title-w-btn">
                                <h3 class="title">
                                    <img src="{{asset('img/'. $apartment->image)}}" class="img-responsive" width="90px" height="90px" />
                                </h3>
                                <div class="btn-group">
                                    {{ $apartment->location }}
                                </div>
                            </div>
                            <div class="tile-body">
                                <p>{{ $apartment->details }}</p>
                                <div class="tile-title-w-btn">
                                    <p>Type of apartment: {{$apartment->units}}</p>
                                    
                                </div>
                                <br>
                                <p><b>Rates</b></p>
                                @foreach($apartment->rates as $rate)
                                
                                <p>{{$rate->duration}}: Ksh {{$rate->amount}}</p>

                                @endforeach

                            </div>
                            <div class="tile-footer">
                                <div class="btn-group">
                                    <a class="btn btn-primary" href="{{ route('edit_apartment', ['id' => $apartment->id]) }}"><i class="fa fa-lg fa-edit"></i></a>
                                    <a class="btn btn-primary" href="{{ route('delete_apartment', ['id' => $apartment->id]) }}"><i class="fa fa-lg fa-trash"></i></a>
                                        
                                    
                                </div>
                            </div>

                        </div>
                    </div>
                @endforeach
            </div>
        @endforeach

            </div>
        </div>
    </main>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ URL::to('js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::to('js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
@endsection
