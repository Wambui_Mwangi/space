@extends('backend.layouts.base')

@section('content')
    <main class="app-content">
        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <h3 class="tile-title">Add a workspace</h3>
                    <hr>
                    <div class="tile-body">
                        <div class="col-md-12">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{route('store_space')}} " method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <p>Select workspace image</p>
                                <div class="custom-file">

                                  <input type="file" class="custom-file-input" id="customFile" name="image">
                                  <label class="custom-file-label" for="customFile">Choose workspace image</label>
                                </div>
                                <div class="form-group">
                                
                                <div class="form-group">
                                    <label class="control-label">Workspace location</label>
                                    <input class="form-control" placeholder="Enter the location of the space" type="text" name="location" value="{{ old('location') }}">
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                    <label class="control-label">Google Pin</label>
                                    <input class="form-control" placeholder="Copy and paste the space's Google Maps pin here" type="text" name="pin" value="{{ old('pin') }}">
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                    <label class="control-label">Workspace Size</label>
                                    <input class="form-control" placeholder="Enter the size  of the space eg 80 x 100" type="text" name="dimensions" value="{{ old('dimensions') }}">
                                </div>
                               
                                <h5>Rates</h5>
                                <div class="form-group" id="fieldList">
                                    <label class="control-label">Charges</label>
                                    <input class="form-control" placeholder="Enter the charges/rates " type="number" name="amount[]" value="{{ old('amount') }}">

                                    <label class="control-label">Duration</label>
                                    <input class="form-control" placeholder="Enter the duration for the rate eg 1 week, 1 day " type="text" name="duration[]" value="{{ old('duration') }}">
                                </div>

                                </div>
                                <div id="new"></div>

                                <div class=" ">
                                    <button class="btn btn-warning" id="addMore">Add more rates</button>
                                    
                                </div>
                                <br><br>
                                <div class="">
                                    <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Add a workspace</button></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('scripts')
<script type="application/javascript">
    $('input[type="file"]').change(function(e){
        var fileName = e.target.files[0].name;
        $('.custom-file-label').html(fileName);
    });
</script>
<script type="text/javascript">
    $(function() {
  $("#addMore").click(function(e) {
    e.preventDefault();
    $("#fieldList:first").clone().appendTo("#new");
  });
});
</script>
@endsection