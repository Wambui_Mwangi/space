@extends('backend.layouts.base')

@section('content')
    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fa fa-users"></i> Workspaces</h1>
            </div>
]            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('add_space') }}">Add a workspace</a></li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="row">
                        <div class="col-md-12">
                            <div id="message" class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        </div>
                    </div>
                @endif
                <div class="tile">
                    <div class="table-responsive">
                        <table class="table  table-hover table-bordered" id="sampleTable">
                            <thead>
                            <tr>
                                <th>Location</th>
                                <th>Size</th>
                                 <th>Pin</th>
                                 <th>Edit details</th>
                                 <th>Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($spaces as $space)
                                <tr>
                                    <td>{{$space->location}}</td>
                                    <td>{{$space->dimensions}} </td>
                                    <td>{{$space->pin}}</td>
                                    <td>
                                        <a href="{{route('edit_space', ['id' => $space->id])}}">Edit space details</a>
                                    </td>
                                    <td>
                                        <a href="{{route('delete_space', ['id' => $space->id])}}">Delete space</a>
                                    </td>
                                </tr>
                            @empty
                                <p>No workspaces available yet!</p>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ URL::to('js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::to('js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
@endsection
