<!-- Sidebar menu-->
<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
    <ul class="app-menu">
        <li><a class="app-menu__item" href=" {{route('home')}}"><i class="app-menu__icon fa fa-home"></i><span class="app-menu__label">Home</span></a></li>
        @if(Auth::user()->role == 'agent' || Auth::user()->role == 'owner')
        <li><a class="app-menu__item" href=" {{route('properties')}}"><i class="app-menu__icon fa fa-building"></i><span class="app-menu__label">Properties</span></a></li>
        <li><a class="app-menu__item" href=" {{route('tenants')}}"><i class="app-menu__icon fa fa-user"></i><span class="app-menu__label">Tenants</span></a></li>
        <li><a class="app-menu__item" href=" {{route('view_tenant_inquiries')}}"><i class="app-menu__icon fa fa-user"></i><span class="app-menu__label">Tenants inquiries</span></a></li>
            @if(Auth::user()->role == 'admin' || Auth::user()->role == 'agent')
            <li><a class="app-menu__item" href=" {{route('owners')}}"><i class="app-menu__icon fa fa-user"></i><span class="app-menu__label">Owners</span></a></li>
            
            
            @endif
        @endif
        @if(Auth::user()->role == 'admin')
        <li><a class="app-menu__item" href=" {{route('clients')}}"><i class="app-menu__icon fa fa-building"></i><span class="app-menu__label">Clients</span></a></li>
        <li><a class="app-menu__item" href=" {{route('properties')}}"><i class="app-menu__icon fa fa-building"></i><span class="app-menu__label">Properties</span></a></li>
        <li><a class="app-menu__item" href=" {{route('users')}}"><i class="app-menu__icon fa fa-user"></i><span class="app-menu__label">Users</span></a></li>
        <li><a class="app-menu__item" href=" {{route('apartments')}}"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">Furnished Apartments</span></a></li>
        <li><a class="app-menu__item" href=" {{route('lands')}}"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">Land</span></a></li>
        <li><a class="app-menu__item" href=" {{route('offices')}}"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">Offices</span></a></li>
        <li><a class="app-menu__item" href=" {{route('spaces')}}"><i class="app-menu__icon fa fa-users"></i><span class="app-menu__label">Workspaces</span></a></li>
        @endif
        @if(Auth::user()->role == 'tenant')
        <li><a class="app-menu__item" href=" {{route('inquiry')}}"><i class="app-menu__icon fa fa-user"></i><span class="app-menu__label">Contact Us</span></a></li>
        
        @endif
        
        
        
        
        <li><a class="app-menu__item" href=" "
               onclick="event.preventDefault();
                 document.getElementById('logout-form').submit();"><i class="app-menu__icon fa fa-sign-out fa-lg"></i><span class="app-menu__label">Logout</span></a>

            <form id="logout-form" action=" " method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
</aside>