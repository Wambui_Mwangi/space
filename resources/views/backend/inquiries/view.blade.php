@extends('backend.layouts.base')

@section('content')
    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fa fa-building"></i> Tenant inquiries</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="row">
                        <div class="col-md-12">
                            <div id="message" class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        </div>
                    </div>
                @endif
                <div class="tile">
                    <div class="table-responsive">
                        <table class="table  table-hover table-bordered" id="sampleTable">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Building</th>
                                <th>Phone</th>
                                
                                <th>Email</th>
                                <th>Message</th>
                                
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($inquiries as $inquiry)
                                <tr>
                                    <td>{{$inquiry->tenant->name}}</td>

                                    <td>{{$inquiry->tenant->occupancy->room->property->name}} {{$inquiry->tenant->occupancy->room->name}}</td>
                                    <td>{{$inquiry->tenant->telephone}}</td>

                                    <td>{{$inquiry->tenant->email}}</td>
                                    <td>{{$inquiry->message}}</td>
                                </tr>
                            @empty
                                <p>No inquiries yet!</p>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ URL::to('js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::to('js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
@endsection
