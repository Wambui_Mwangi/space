@extends('backend.layouts.base') 

@section('content')
    <main class="app-content">
        <div class="row">
            <div class="col-md-6 col-lg-6">
                <div class="widget-small primary coloured-icon"><i class="icon fa fa-users fa-3x"></i>
                    <div class="info">
                        <h4>Properties</h4>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-6">
                <div class="widget-small info coloured-icon"><i class="icon fa fa-users fa-3x"></i>
                    <div class="info">
                        <h4>Property Owners</h4>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-6">
                <div class="widget-small warning coloured-icon"><i class="icon fa fa-users fa-3x"></i>
                    <div class="info">
                        <h4>Tenants</h4>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-6">
                <div class="widget-small danger coloured-icon"><i class="icon fa fa-users fa-3x"></i>
                    <div class="info">
                        <h4>Agents</h4>
                        
                    </div>
                </div>
            </div>
            
        </div>
       <!--  <div class="row">
            <div class="col-md-6">
                <div class="tile">
                    <h4 class="tile-title folder-head">Sales Reports</h4>
                    <div class="table-responsive mailbox-messages">
                        <table class="table table-hover">
                            <tbody>
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="tile">
                    <h4 class="tile-title folder-head">Clients</h4>
                    <div class="table-responsive mailbox-messages">
                        <table class="table table-hover">
                            <tbody>
                            
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div> -->
    </main>
@endsection
\