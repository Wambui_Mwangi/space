@extends('backend.layouts.base')

@section('content')
    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fa fa-building"></i> {{$property->name}} rooms</h1>
            </div>
            @if(Auth::user()->role=='admin' || Auth::user()->role=='agent')
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('add_room', ['id' => $property->id]) }}">Add room</a></li>
            </ul>
            @endif
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="row">
                        <div class="col-md-12">
                            <div id="message" class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        </div>
                    </div>
                @endif
                <div class="tile">
                    <div class="table-responsive">
                        <table class="table  table-hover table-bordered" id="sampleTable">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Occupancy</th>
                                <th>Edit room name</th>
                                <th>Delete room</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($rooms as $room)
                                <tr>
                                    <td>{{$room->name}}</td>
                                    
                                    <td><a href="{{ route('occupancy', ['id' => $room->id]) }}">View occupancy</a>

                                    <td><a href="{{ route('edit_room', ['id' => $room->id]) }}">Edit room details</a></td>
                                    <td><a href="{{route('delete_room', ['id' => $room->id])}}">Delete room</a></td>
                                </tr>
                            @empty
                                <p>No rooms yet!</p>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ URL::to('js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::to('js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
@endsection
