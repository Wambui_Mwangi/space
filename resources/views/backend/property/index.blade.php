@extends('backend.layouts.base')

@section('content')
    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fa fa-users"></i> Properties</h1>
            </div>
            @if(Auth::user()->role=='admin' || Auth::user()->role=='agent')
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('add_property') }}">Add property</a></li>
            </ul>
            @endif
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="row">
                        <div class="col-md-12">
                            <div id="message" class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        </div>
                    </div>
                @endif
                <div class="tile">
                    <div class="table-responsive">
                        <table class="table  table-hover table-bordered" id="sampleTable">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Location</th>
                                 <th>Category</th>
                                 <th>Rooms</th>
                                 <th>Price</th>
                               @if($user->role == 'admin' || $user->role == 'agent')
                                
                                <th>Featured</th>
                                <th>Edit property</th>
                                <th>Delete property</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($properties as $property)
                                <tr>
                                    <td>{{$property->name}}</td>
                                    <td>{{$property->location}} in {{$property->county->name}} county</td>
                                    <td>{{$property->category->name}}</td>
                                    @if($property->category->name == 'Rent')
                                    <td>
                                        <a href="{{route('rooms', ['id' => $property->id])}}">View property rooms</a>
                                    </td>
                                    @else
                                    <td>
                                        No rooms available
                                    </td>
                                    @endif
                                    <td>{{$property->price}}</td>
                                    @if($user->role == 'admin' || $user->role == 'agent')
                                    
                                    
                                    <td>
                                    @if($property->featured == 0)
                                    Not featured
                                    <a href="{{route('feature_property', ['id' => $property->id])}}"><br>Feature property</a>
                                    @else
                                    Featured
                                    <a href="{{route('feature_property', ['id' => $property->id])}}">Unfeature property</a>
                                    @endif
                                    </td>
                                    
                                    <td><a href="{{ route('edit_property', ['id' => $property->id]) }}">Edit property details</a></td>
                                    <td><a href="{{route('delete_property', ['id' => $property->id])}}">Delete property</a></td>
                                    @endif
                                </tr>
                            @empty
                                <p>No properties yet!</p>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ URL::to('js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::to('js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
@endsection
