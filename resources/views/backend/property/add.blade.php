@extends('backend.layouts.base')

@section('content')
    <main class="app-content">
        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <h3 class="tile-title">Add property</h3>
                    <hr>
                    <div class="tile-body">
                        <div class="col-md-12">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{route('store_property')}} " method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <div class="form-group">
                                    <label class="control-label">Property name</label>
                                    <input class="form-control" placeholder="Enter the name of the property" type="text" name="name" value="{{ old('name') }}">
                                </div>

                                    <label class="control-label">Property owner</label>
                                    <select class="form-control" type="number" name="owner_id" value="{{ old('owner_id') }}">
                                        <option value=" ">None</option>
                                        @foreach($owners as $owner)
                                            <option value="{{$owner->id}}">{{$owner->name}}</option>
                                        @endforeach
                                    </select>

                                    <label class="control-label">Property agent</label>
                                    <select class="form-control" type="number" name="agent_id" value="{{ old('agent_id') }}">
                                        <option value=" ">None</option>
                                        @foreach($agents as $agent)
                                            <option value="{{$agent->id}}">{{$agent->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Category</label>
                                    <select class="form-control" type="number" name="category_id" value="{{ old('category_id') }}">
                                        <option value=" ">None</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">County</label>
                                    <select class="form-control" type="number" name="county_id" value="{{ old('county_id') }}">
                                        <option value=" ">None</option>
                                        @foreach($counties as $county)
                                            <option value="{{$county->id}}">{{$county->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Location</label>
                                    <input class="form-control" placeholder="Enter the location of the property" type="text" name="location" value="{{ old('location') }}">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Description</label>
                                    <input class="form-control" placeholder="Enter additional information about the property eg bedrooms, space etc" type="integer" name="description" value="{{ old('description') }}">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Price</label>
                                    <input class="form-control" placeholder="Enter the price" type="number" name="price" value="{{ old('price') }}">
                                </div>
                                <p>Select property image</p>
                                <div class="custom-file">

                                  <input type="file" class="custom-file-input" id="customFile" name="image">
                                  <label class="custom-file-label" for="customFile">Choose property image</label>
                                </div>
                                <br><br>
                                <div class="">
                                    <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Add property</button></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('scripts')
<script type="application/javascript">
    $('input[type="file"]').change(function(e){
        var fileName = e.target.files[0].name;
        $('.custom-file-label').html(fileName);
    });
</script>
@endsection