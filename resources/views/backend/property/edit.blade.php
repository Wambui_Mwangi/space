@extends('backend.layouts.base') 

@section('content')
    <main class="app-content">
        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <h3 class="tile-title">Update property information</h3>
                    <hr>
                    <div class="tile-body">
                        <div class="col-md-12">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{route('update_property', ['id' => $property->id])}} " method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label class="control-label">Property owner</label>
                                    <select class="form-control" type="number" name="user_id">
                                        <option value="{{$property->user_id}} ">{{$property->user->name}}</option>
                                        @foreach($users as $user)
                                            <option value="{{$user->id}}">{{$user->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Category</label>
                                    <select class="form-control" type="number" name="category_id" >
                                        <option value="{{$property->category_id}} ">{{$property->category->name}}</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">County</label>
                                    <select class="form-control" type="number" name="county_id">
                                        <option value="{{ $property->county_id }} ">{{$property->county->name}}</option>
                                        @foreach($counties as $county)
                                            <option value="{{$county->id}}">{{$county->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Location</label>
                                    <input class="form-control" placeholder="Enter the location of the property" type="text" name="location" value="{{ $property->location }}">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Description</label>
                                    <input class="form-control" placeholder="Enter additional information about the property eg bedrooms, space etc" type="integer" name="description" value="{{ $property->description }}">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Price</label>
                                    <input class="form-control" placeholder="Enter the price" type="number" name="price" value="{{ $property->price }}">
                                </div>
                                <p>Select property image</p>
                                <div class="custom-file">

                                  <input type="file" class="custom-file-input" id="customFile" name="image">
                                  <label class="custom-file-label" for="customFile">Choose property image 
                                    <span class="custom-file-control form-control-file"></span>
                                </label>
                                </div>
                                <br><br>
                                <div class="">
                                    <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update property</button></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('scripts')
<script type="application/javascript">
    $('input[type="file"]').change(function(e){
        var fileName = e.target.files[0].name;
        $('.custom-file-label').html(fileName);
    });
</script>
@endsection