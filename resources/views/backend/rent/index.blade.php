@extends('backend.layouts.base')

@section('content')
    <main class="app-content">
        <div class="app-title">
            <div>
                <h1><i class="fa fa-users"></i> Rent details for {{$occupant->room->name}}</h1>
            </div>
            @if(Auth::user()->role=='admin' || Auth::user()->role=='agent')
            <ul class="app-breadcrumb breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('add_rent', ['id' => $occupant->id]) }}">Add rent</a></li>
            </ul>
            @endif
        </div>
        <div class="row">
            <div class="col-md-12">
                @if(Session::has('success'))
                    <div class="row">
                        <div class="col-md-12">
                            <div id="message" class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        </div>
                    </div>
                @endif
                <div class="tile">
                    <div class="table-responsive">
                        <table class="table  table-hover table-bordered" id="sampleTable">
                            <thead>
                            <tr>
                                <th>Time</th>
                                <th>Amount</th>
                                <th>Being payment for</th>
                                 
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($rents as $rent)
                                <tr>
                                    <td>{{$rent->month}} {{$rent->year}}</td>
                                    <td>{{$rent->amount}} </td>
                                    <td>{{$rent->details}}</td>
                                    
                                </tr>
                            @empty
                                <p>No rent paid yet</p>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection

@section('scripts')
    <script type="text/javascript" src="{{ URL::to('js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::to('js/dataTables.bootstrap.min.js') }}"></script>
    <script type="text/javascript">$('#sampleTable').DataTable();</script>
@endsection
