@extends('backend.layouts.base')

@section('content')
    <main class="app-content">
        <div class="row">
            <div class="col-md-12">
                <div class="tile">
                    <h3 class="tile-title">Add rent</h3>
                    <hr>
                    <div class="tile-body">
                        <div class="col-md-12">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <form action="{{route('store_rent', ['id' => $occupancy->id])}} " method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="control-label">Amount paid</label>
                                        <input class="form-control" placeholder="Enter the amount" type="number" name="amount" value="{{ old('amount') }}">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Details</label>
                                        <input class="form-control" placeholder="Enter what the amount paid caters for eg rent, water etc" type="text" name="details" value="{{ old('details') }}">
                                    </div>

                                    <label class="control-label">Month paid</label>
                                    <select class="form-control" type="text" name="month" value="{{ old('month') }}">
                                        <option value="January">January</option>
                                        <option value="February">February</option>
                                        <option value="March">March</option>
                                        <option value="April">April</option>
                                        <option value="May">May</option>
                                        <option value="June">June</option>
                                        <option value="July">July</option>
                                        <option value="August">August</option>
                                        <option value="September">September</option>
                                        <option value="October">October</option>
                                        <option value="November">November</option>
                                        <option value="December">December</option>
                                    </select>


                                    <label class="control-label">Year</label>
                                    <select class="form-control" type="number" name="year" value="{{ old('year') }}">
                                        <option value=" ">None</option>
                                        @for ($i = $year-2; $i < $year+2; $i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>

                                    
                                
                                <br><br>
                                <div class="">
                                    <button class="btn btn-primary" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Add rent </button></a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
@section('scripts')
<script type="application/javascript">
    $('input[type="file"]').change(function(e){
        var fileName = e.target.files[0].name;
        $('.custom-file-label').html(fileName);
    });
</script>
@endsection