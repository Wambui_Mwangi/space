<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('county_id');
            $table->unsignedBigInteger('owner_id');
            $table->unsignedBigInteger('agent_id');
            $table->string('location');
            $table->string('description');
            $table->integer('price');
            $table->integer('featured')->default(0)->comment('1 for featured');
            $table->string('image');
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('county_id')->references('id')->on('counties');
            $table->foreign('owner_id')->references('id')->on('users');
            $table->foreign('agent_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
