<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Traits\HasRoles;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create super user
        
        $user = User::create([
            'name' => 'Wambui Mwangi',
            'email' => 'charity@gmail.com',
            'password' => bcrypt('charity@gmail.com'),
            'telephone' => '0729922992',
            'is_active' => 1,
            'role' => 'admin',
        ]);


    }
}
